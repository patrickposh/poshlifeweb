import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { UserData } from 'src/app/app.models';
import { LoginService } from 'src/app/login.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {
  public user:UserData;
  message:string;
  pImage: string;
  constructor(public appService:AppService, public loginService: LoginService, public router: Router,public translate: TranslateService) { }

  ngOnInit() {
    this.appService.currentMessage.subscribe(message => 
      this.user = this.loginService.getUser())
  }

  ngAfterViewInit() {
    this.appService.currentImage.subscribe(profileImage => this.pImage = profileImage);
  }

  onclickSignout() {
    this.loginService.removeUser();
    this.user = this.loginService.getUser();
    this.router.navigate(['/login']);
  }
}
