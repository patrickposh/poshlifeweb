import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-lang',
  templateUrl: './lang.component.html',
  styleUrls: ['./lang.component.scss']
})
export class LangComponent implements OnInit {
  public flags = [
    { name:'English', image: 'assets/images/flags/gb.svg',value: 'en' },
    { name:'española', image: 'assets/images/flags/de.svg' ,value: 'sp'},
    { name:'हिंदी', image: 'assets/images/flags/ru.svg' ,value: 'hn'},
    { name:'Française', image: 'assets/images/flags/fr.svg' ,value: 'fr'},
    { name:'عربى', image: 'assets/images/flags/tr.svg' ,value: 'ar'},
    { name:'中文', image: 'assets/images/flags/gb.svg' ,value: 'ch'},
    { name:'فارسی', image: 'assets/images/flags/gb.svg' ,value: 'per'}
  ]
  public flag:any;
  constructor(public translate: TranslateService) { }

  ngOnInit() {
    for (let i = 0; i < this.flags.length; i++) {
      if(localStorage.getItem('language') == this.flags[i].value){
        this.flag = this.flags[i];
        break;
      }
    }
    if(!this.flag){
      this.flag = this.flags[0];
    }
  }

  public changeLang(flag){
    this.flag = flag;
    this.translate.use(flag.value);
    localStorage.setItem('language',flag.value);
    if(flag.value == "ar" || flag.value == "per"){
      document.documentElement.dir = "rtl";
    }else{
      document.documentElement.dir = "ltr";
    }
  }
}
