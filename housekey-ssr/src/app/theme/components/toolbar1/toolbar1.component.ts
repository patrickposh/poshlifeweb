import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatOptionSelectionChange } from '@angular/material/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { debounceTime, filter, finalize, switchMap, tap } from 'rxjs/operators';
import { LocationC, SearchObj } from 'src/app/app.models';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-toolbar1',
  templateUrl: './toolbar1.component.html'
})
export class Toolbar1Component implements OnInit {
  @Output() onMenuIconClick: EventEmitter<any> = new EventEmitter<any>();
  @Input() homeToolbar:string = "";
  
  public searchValues = [];
  public searchText = "";
  public searchoptions: any[] = [];
  public form1: FormGroup;
  public searchObjNew:SearchObj;
  public currentLoc:LocationC;
  userType: string;
  cState: string;
  pImage: string;
  isLoading: boolean;

  constructor(public fb: FormBuilder,public appService:AppService,public router:Router, private cdr: ChangeDetectorRef,public translate: TranslateService) {
    this.currentLoc = {
      id: 1,
      lat: 43.63943345,
      lng: -79.39972759226308};
   }
  ngAfterViewInit() {
    this.appService.currentState.subscribe(stateName => this.cState = stateName);
    this.appService.currentImage.subscribe(profileImage => this.pImage = profileImage);
    this.appService.currentRole.subscribe(userRole => this.userType = userRole);
    this.cdr.detectChanges();
    this.form1.get("myControl").valueChanges
    .pipe(
      filter(text => text.length > 2),
      debounceTime(500),
      tap(() => {
        this.searchoptions = [];
        this.isLoading = true;
      }),
      switchMap(value => this.appService.searchAddress(this.getSearchFilt(value))
        .pipe(
          finalize(() => {
            //this.onSearchChange.emit(this.form);
            this.isLoading = false
          }),
        )
      )
    )
    .subscribe(data => {
      if (data.data == undefined) {
        this.searchoptions = [];
      } else {
        if(data.data && data.data.length > 0){
          this.searchValues = [{
              name: 'Locations',
              data: [data.data[0]]
            },{
              name: 'Listing',
              data: data.data
            }
          ]
          this.searchoptions = data.data;
        }
      }
    });
    //throw new Error("Method not implemented.");
  }

  getOptionText(option) {
    if(option){
      return option.locationSearch;
    }else{
      return '';
    }
  }
  
  optionSelected(event: MatOptionSelectionChange) {
    this.form1.patchValue({
      myControl: this.searchText
    })
      if(event.source.group.label.includes("Locations")){
        if(event.source.value.longitude && event.source.value.latitude){
          this.currentLoc.lat = event.source.value.latitude;
          this.currentLoc.lng = event.source.value.longitude;
          this.appService.setLocation(this.currentLoc);
        }
        //this.appService.setLatLong(2,3);
        this.router.navigate(['/properties']);
      }else if(event.source.group.label.includes("Listing")){
        this.router.navigate(['/properties',event.source.value.id]);
      }
  }

  ngOnInit() { 
    this.form1 = this.fb.group({
      myControl: new FormControl()
    })
  }

  private getSearchFilt(text:string){
    return {"currentPage":1,"searchText":text}
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.searchoptions.filter(option => option.toLowerCase().includes(filterValue));
  }

  public sidenavToggle(){
    this.onMenuIconClick.emit();
  }

  public sidenavToggle1(){
  }

  public getAppearance(){
    return '';
  }

  public getFloatLabel(){
    return '';
  }
}