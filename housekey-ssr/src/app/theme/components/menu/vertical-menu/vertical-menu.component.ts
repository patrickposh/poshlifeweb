import { Component, OnInit, Input } from '@angular/core';
import { MenuService } from '../menu.service';
import { Menu } from '../menu.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-vertical-menu',
  templateUrl: './vertical-menu.component.html',
  styleUrls: ['./vertical-menu.component.scss'],
  providers: [ MenuService ]
})
export class VerticalMenuComponent implements OnInit {
  @Input('menuParentId') menuParentId;
  public menuItems: Array<Menu>;
  public flags = [
    { name:'English', image: 'assets/images/flags/gb.svg',value: 'en' },
    { name:'española', image: 'assets/images/flags/de.svg' ,value: 'sp'},
    { name:'हिंदी', image: 'assets/images/flags/ru.svg' ,value: 'hn'},
    { name:'Française', image: 'assets/images/flags/fr.svg' ,value: 'fr'},
    { name:'عربى', image: 'assets/images/flags/tr.svg' ,value: 'ar'},
    { name:'中文', image: 'assets/images/flags/gb.svg' ,value: 'ch'},
    { name:'فارسی', image: 'assets/images/flags/gb.svg' ,value: 'per'}
  ];
  public flag:any;
  constructor(public menuService:MenuService,public translate: TranslateService) { }

  ngOnInit() {
    this.menuItems = this.menuService.getVerticalMenuItems();
    this.menuItems = this.menuItems.filter(item => item.parentId == this.menuParentId);
    for (let i = 0; i < this.flags.length; i++) {
      if(localStorage.getItem('language') == this.flags[i].value){
        this.flag = this.flags[i];
        break;
      }
    }
    if(!this.flag){
      this.flag = this.flags[0];
    }
  }

  public changeLang(flag){
    this.flag = flag;
    this.translate.use(flag);
    localStorage.setItem('language',flag);
    if(flag == "ar" || flag == "per"){
      document.documentElement.dir = "rtl";
    }else{
      document.documentElement.dir = "ltr";
    }
  }

  onClick(menuId){
    this.menuService.toggleMenuItem(menuId);
    this.menuService.closeOtherSubMenus(this.menuService.getVerticalMenuItems(), menuId);    
  }

}
