import { Menu } from './menu.model';

export const horizontalMenuItems = [ 
    new Menu (1, 'Home', '/', null, null, false, 0),
    new Menu (2, 'Properties', '/properties', null, null, false, 0), 
    // new Menu (40, 'Pages', null, null, null, true, 0),
    // new Menu (41, 'Agents', '/agents', null, null, false, 40), 
    // new Menu (42, 'Agent', '/agents/1', null, null, false, 40),
    new Menu (43, 'Login', '/login', null, null, false, 40), 
    new Menu (44, 'Register', '/register', null, null, false, 40), 
    new Menu (45, 'FAQs', '/faq', null, null, false, 40),
    new Menu (46, 'Pricing', '/pricing', null, null, false, 40), 
    new Menu (47, 'Terms & Conditions', '/terms-conditions', null, null, false, 40), 
    new Menu (48, 'Landing', '/landing', null, null, false, 40),  
    new Menu (50, '404 Page', '/404', null, null, false, 40),  
    new Menu (60, 'Contact', '/contact', null, null, false, 0),  
    new Menu (70, 'About Us', '/about', null, null, false, 0),   
    new Menu (140, 'Others', null, null, null, true, 40),
    // new Menu (141, 'External Link', null, 'http://themeseason.com', '_blank', false, 140), 
    new Menu (142, 'Menu item', null, 'http://themeseason.com', '_blank', false, 140),
    new Menu (143, 'Menu item', null,'http://themeseason.com', '_blank', false, 140),
    new Menu (144, 'Menu item', null,'http://themeseason.com', '_blank', false, 140)    
]

export const verticalMenuItems = [ 
    new Menu (2, 'Map Search', '/properties', null, null, false, 0), 
    new Menu (90, 'Language', null, null, null, true, 0),
    new Menu (41, 'Blogs', '/blogs', null, null, false, 0),
    new Menu (40, 'Account', '/account/profile', null, null, false, 0), 
    new Menu (70, 'About Us', '/about', null, null, false, 0),
    new Menu (43, 'Logout', '/login', null, null, false, 0), 
    new Menu (43, 'Login', '/login', null, null, false, 40), 
    new Menu (44, 'Register', '/register', null, null, false, 40),   
    new Menu (45, 'FAQs', '/faq', null, null, false, 40),
    new Menu (46, 'Pricing', '/pricing', null, null, false, 40), 
    new Menu (47, 'Terms & Conditions', '/terms-conditions', null, null, false, 40),  
    new Menu (48, 'Landing', '/landing', null, null, false, 40), 
    new Menu (50, '404 Page', '/404', null, null, false, 40), 
    new Menu (91, 'English', null, null, 'en', false, 90),
    new Menu (92, 'española', null, null, 'sp', false, 90),
    new Menu (93, 'हिंदी', null, null, 'hn', false, 90),
    new Menu (94, 'عربى', null, null, 'ar', false, 90),
    new Menu (95, '中文', null, null, 'ch', false, 90),
    new Menu (96, 'فارسی', null, null, 'per', false, 90),
    new Menu (97, 'Française', null, null, 'fr', false, 90),
]

