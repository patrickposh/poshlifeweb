import { Component, PLATFORM_ID, Inject } from '@angular/core';
import { Settings, AppSettings } from './app.settings';
import { Router, NavigationEnd } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { AppService } from './app.service';
import { LoginService } from './login.service';
import { UserData } from './app.models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public user:UserData;
  public settings: Settings;
  constructor(public appSettings:AppSettings, public router:Router,
              @Inject(PLATFORM_ID) private platformId: Object,private translateService: TranslateService,public appService:AppService, public loginService: LoginService){
    this.settings = this.appSettings.settings;
    // localStorage.setItem('language',"");
    if(localStorage.getItem('language')){
      translateService.setDefaultLang(localStorage.getItem('language'));
      if(localStorage.getItem('language') == "ar" || localStorage.getItem('language') == "per"){
        document.documentElement.dir = "rtl";        
      }else{
        document.documentElement.dir = "ltr";
      }
    }else{
      translateService.setDefaultLang('en');
    }
  }

  ngAfterViewInit(){ 
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {   
        setTimeout(() => {
          if (isPlatformBrowser(this.platformId)) {
            window.scrollTo(0,0);
          }
        }); 
      }            
    });  
    this.user = this.loginService.getUser();
      if(this.user){
        if(this.user.userVM){
          this.appService.changeProfileImage(this.user.userVM.imageName);
        }else{
          this.appService.changeProfileImage('');
        }
        this.appService.changeUserRole(this.user.userVM.role);
      }  
  }

}
