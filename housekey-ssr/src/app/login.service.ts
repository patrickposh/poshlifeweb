import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';
// import 'rxjs/add/Observable/throw';
import { UserData, UserVM } from './app.models';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
public urlNew = "https://property.mindnerves.com/";
	constructor(public http:HttpClient) { }
  public userToken: UserData;
  public login(obj:any): Observable<any>{
    return this.http.post(this.urlNew + 'api/login',obj);
  }

  public socialLogin(obj:any): Observable<any>{
    let getHeaders: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(this.urlNew + 'api/socialLogin',obj, {headers: getHeaders});
  }

  public forgotPassword(obj:any): Observable<any>{
    return this.http.post(this.urlNew + 'api/forgetPassword',obj);
  }

  public uploadUserPicture(obj:any,token:string): Observable<any>{
    let getHeaders: HttpHeaders = new HttpHeaders({
      'Authorization':token
    });
    var imageData = new FormData();
    imageData.append("file",obj);
    return this.http.post(this.urlNew + 'api/user/fileUploader',imageData, {headers: getHeaders});
  }

  public updateUser(obj:any,token:string): Observable<any>{
    let getHeaders: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization':token
    });
    return this.http.put(this.urlNew + 'api/user/',obj, {headers: getHeaders});
  }
  
  public setUser(user:UserData){
    this.userToken = user;
    localStorage.setItem('user',JSON.stringify(user));
    
  }

  public removeUser(){
    localStorage.removeItem('user');
  }
  
  public getUser(){
    var temp = localStorage.getItem('user');
    if(temp){
      var user:UserData = JSON.parse(temp);
      return user
    }
    return null;
  }

  public activate(obj:any,token:string): Observable<any>{
    let getHeaders: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization':token
    });
    return this.http
    .post(this.urlNew + 'api/user/activateUser', obj, {headers: getHeaders});
    // return this.http.post([{options:{header:headers}] ,this.urlNew + 'api/user/activateUser',obj);
  }
  public reset(obj:any,token:string): Observable<any>{
    let getHeaders: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization':token
    });
    return this.http
    .post(this.urlNew + 'api/user/resetPassword', obj, {headers: getHeaders});
    // return this.http.post([{options:{header:headers}] ,this.urlNew + 'api/user/activateUser',obj);
  }

  public register(obj:any): Observable<any>{
    return this.http.post(this.urlNew +'api/create',obj);
  }
}
