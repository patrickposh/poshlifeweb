import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser'
import { Settings, AppSettings } from '../../app.settings';

@Component({
  selector: 'app-header-image',
  templateUrl: './header-image.component.html',
  styleUrls: ['./header-image.component.scss']
})
export class HeaderImageComponent implements OnInit {
  @Input('backgroundImage') backgroundImage;
  @Input('bgImageAnimate') bgImageAnimate;
  @Input('contentOffsetToTop') contentOffsetToTop;
  @Input('contentMinHeight') contentMinHeight;
  @Input('title') title;
  @Input('desc') desc;
  @Output() onTypeChangeClick: EventEmitter<String> = new EventEmitter<String>();
  @Input('isHomePage') isHomePage:boolean = false;
  public bgImage;
  public settings: Settings;
  public isResidential: boolean;
  constructor(public appSettings:AppSettings, private sanitizer:DomSanitizer) {
    this.settings = this.appSettings.settings;
    this.settings.headerBgImage = true;
    this.isResidential = true;
  }

  ngOnInit() {
    if(this.contentOffsetToTop)
      this.settings.contentOffsetToTop = this.contentOffsetToTop;
    if(this.backgroundImage) 
      this.bgImage = this.sanitizer.bypassSecurityTrustStyle('url('+this.backgroundImage +')'); 
  }

  changeType(){
    if(this.isResidential){
      this.onTypeChangeClick.emit('Residential');
    }else{
      this.onTypeChangeClick.emit('Commercial');
    }
    
    this.isResidential = !this.isResidential;
  }
  ngOnDestroy(){    
    this.settings.headerBgImage = false; 
    this.settings.contentOffsetToTop = false;
  }
}
