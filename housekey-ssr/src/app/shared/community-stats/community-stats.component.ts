import { Component, OnInit, AfterViewInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-community-stats',
  templateUrl: './community-stats.component.html',
  styleUrls: ['./community-stats.component.scss']
})

export class CommunityStatsComponent implements OnInit, AfterViewInit {

  public dataPoints1 = [];
  public dataPoints2 = [];
  public surveyData1 = [
    { name: 'Att/Row/Twnhouse', value: 450 },
    { name: 'Condo Apt', value: 300 },
    { name: 'Detached', value: 800 },
    { name: 'Link', value: 150 },
    { name: 'Semi-Detached', value: 150 },
    { name: 'Condo Townhouse', value: 250 },
    { name: 'Other', value: 230 }
  ];
  public multi = [
      {
        "name": "Detached",
        "series": [
          {
            "name": "2014",
            "value": 1100000
          },
          {
            "name": "2015",
            "value": 1200000
          },
          {
            "name": "2016",
            "value": 1100000
          },
          {
            "name": "2017",
            "value": 1500000
          },
          {
            "name": "2018",
            "value": 1300000
          },
          {
            "name": "2019",
            "value": 1200000
          },
          {
            "name": "2020",
            "value": 1400000
          }
        ]
      },
      {
        "name": "All Properties",
        "series": [
          {
            "name": "2014",
            "value": 300000
          },
          {
            "name": "2015",
            "value": 1000000
          },
          {
            "name": "2016",
            "value": 800000
          },
          {
            "name": "2017",
            "value": 1170000
          },
          {
            "name": "2018",
            "value": 900000
          },
          {
            "name": "2019",
            "value": 1050000
          },
          {
            "name": "2020",
            "value": 1100000
          }
        ]
      }
  ];
  public surveyData = [
    { name: '700k', value: 1 },
    { name: '800k', value: 2 },
    { name: '900k', value: 3 },
    { name: '1000k', value: 1 },
    { name: '1100k', value: 5 },
    { name: '1200k', value: 2 },
    { name: '1300k', value: 3 },
    { name: '1400k', value: 1 },
    { name: '1500k', value: 0 },
    { name: '1600k', value: 5 },
    { name: '1700k', value: 1 },
    { name: '1800k', value: 2 },
    { name: '1900k', value: 1 },
    { name: '2000k', value: 0 },
    { name: '2100k', value: 0 },
    { name: '2200k', value: 4 },
    { name: '2300k', value: 2 },
    { name: '2400k', value: 6 },
    { name: '2500k', value: 2 },
    { name: '2600k', value: 5 },
    { name: '2700k', value: 1 },
    { name: '2800k', value: 3 },
    { name: '2900k', value: 2 },
    { name: '3000k', value: 3 },
    { name: '3100k', value: 2 },
    { name: '3200k', value: 1 },
    { name: '3300k', value: 4 },
    { name: '3400k', value: 3 },
    { name: '3500k', value: 5 },
    { name: '3600k', value: 6 },
    { name: '3700k', value: 4 },
    { name: '3800k', value: 2 },
    { name: '3900k', value: 5 }
  ];
  constructor(public translate: TranslateService) {
    this.dataPoints1 = [
      { x: new Date(2010, 0), y: 700000 },
      { x: new Date(2010, 6), y: 1600000 },
      { x: new Date(2011, 0), y: 1400000 },
      { x: new Date(2011, 6), y: 1200000 },
      { x: new Date(2012, 0), y: 1100000 },
      { x: new Date(2012, 6), y: 1300000 },
      { x: new Date(2013, 0), y: 900000 },
      { x: new Date(2013, 6), y: 1100000 },
      { x: new Date(2014, 0), y: 1200000 },
      { x: new Date(2014, 6), y: 1500000 },
      { x: new Date(2015, 0), y: 1200000 },
      { x: new Date(2015, 6), y: 1100000 },
      { x: new Date(2016, 0), y: 1100000 },
      { x: new Date(2016, 6), y: 1400000 },
      { x: new Date(2017, 0), y: 1100000 },
      { x: new Date(2017, 6), y: 1200000 },
      { x: new Date(2018, 0), y: 1100000 },
      { x: new Date(2018, 6), y: 1400000 },
      { x: new Date(2019, 0), y: 1200000 },
      { x: new Date(2019, 6), y: 1200000 },
      { x: new Date(2020, 0), y: 1000000 },
      { x: new Date(2020, 6), y: 1300000 }
    ]
    this.dataPoints2 = [
      { x: new Date(2010, 0), y: 1500000 },
      { x: new Date(2010, 6), y: 1700000 },
      { x: new Date(2011, 0), y: 1500000 },
      { x: new Date(2011, 6), y: 1300000 },
      { x: new Date(2012, 0), y: 1400000 },
      { x: new Date(2012, 6), y: 1400000 },
      { x: new Date(2013, 0), y: 1100000 },
      { x: new Date(2013, 6), y: 1200000 },
      { x: new Date(2014, 0), y: 1300000 },
      { x: new Date(2014, 6), y: 1600000 },
      { x: new Date(2015, 0), y: 1400000 },
      { x: new Date(2015, 6), y: 1200000 },
      { x: new Date(2016, 0), y: 1300000 },
      { x: new Date(2016, 6), y: 1500000 },
      { x: new Date(2017, 0), y: 1200000 },
      { x: new Date(2017, 6), y: 1400000 },
      { x: new Date(2018, 0), y: 1300000 },
      { x: new Date(2018, 6), y: 1500000 },
      { x: new Date(2019, 0), y: 1300000 },
      { x: new Date(2019, 6), y: 1400000 },
      { x: new Date(2020, 0), y: 1100000 },
      { x: new Date(2020, 6), y: 1700000 }
    ]
   }
  
  ngAfterViewInit(): void {
  }

  
  ngOnInit(): void {
  
  }
}
