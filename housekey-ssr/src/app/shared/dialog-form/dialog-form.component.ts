import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { UserData } from 'src/app/app.models';
import { AppService } from 'src/app/app.service';
import { LoginService } from 'src/app/login.service';

export interface DialogData {
  email: string;
  pid: string;
}


@Component({
  selector: 'app-dialog-form',
  templateUrl: './dialog-form.component.html'
})
export class DialogFormModule {

  public scheduleForm: FormGroup;
  public user:UserData;
  public psConfig: PerfectScrollbarConfigInterface = {
    wheelPropagation:true
  };
  constructor(
    public dialogRef: MatDialogRef<DialogFormModule>,public fb: FormBuilder,public appService:AppService,  public loginService: LoginService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, public snackBar: MatSnackBar,public translate: TranslateService, public router:Router) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.user = this.loginService.getUser();
    this.scheduleForm = this.fb.group({
      name: ['', Validators.required],
      contactNumber: ['', Validators.required], 
      email: [this.data.email, Validators.required],
      // message: ['', Validators.required],
      // userId: this.user.userVM.id,
      propertyId: this.data.pid
    });
  }

  public onScheduleFormSubmit(values:any){
    if (this.scheduleForm.valid){
      this.appService.preAprovedMortgage(values).subscribe(data=>{
        this.scheduleForm.reset();
        this.snackBar.open('Successfully sumited form !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
          Object.keys(this.scheduleForm.controls).forEach(key => {
              this.scheduleForm.get(key).setErrors(null) ;
          });
        this.dialogRef.close();
      },err => {
        if(err.status == 401){
          this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
          setTimeout(() => {
              this.router.navigate(['/login']);
          }, 5000);
        }
      }); 
    }
  }

}
