import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { UserData } from 'src/app/app.models';
import { AppService } from 'src/app/app.service';
import { LoginService } from 'src/app/login.service';

export interface DialogData {
  id: number;
  email: string;
  propertyId: number;
}


@Component({
  selector: 'app-visit-form',
  templateUrl: './visit-form.component.html'
})
export class VisitFormModule {

  public scheduleForm: FormGroup;
  public user:UserData;
  public selected = 'option1';
  public psConfig: PerfectScrollbarConfigInterface = {
    wheelPropagation:true
  };
  constructor(
    public dialogRef: MatDialogRef<VisitFormModule>,public fb: FormBuilder,public appService:AppService,  public loginService: LoginService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, public snackBar: MatSnackBar,public translate: TranslateService,public router:Router) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {console.log("***************3--",this.data.id);
    this.user = this.loginService.getUser();
    this.scheduleForm = this.fb.group({
      message: ['', Validators.required],
      comment: ['', Validators.required], 
      status: ['', Validators.required], 
      scheduleDate: "",
      id: this.data.id,
      propertyId: this.data.propertyId,
      email: this.data.email,
      createdBy: null,
      createdDate: null, 
      modifiedDate: null
    });
  }

  public onScheduleFormSubmit(values:any){ 
    this.appService.updateSchedule(values).subscribe(res => {
      if(res.code == 200 && res.status == "success"){
        this.snackBar.open('Successfully updated.', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
      }
      setTimeout(() => {
        this.dialogRef.close();
      }, 2000);
    },err => {
      if(err.status == 401){
        this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
        setTimeout(() => {
            this.router.navigate(['/login']);
        }, 5000);
      }
    });
  }

}
