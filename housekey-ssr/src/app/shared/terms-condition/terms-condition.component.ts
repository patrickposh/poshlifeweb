import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { UserData } from 'src/app/app.models';
import { AppService } from 'src/app/app.service';
import { LoginService } from 'src/app/login.service';

export interface DialogData {
  email: string;
  pid: string;
}


@Component({
  selector: 'terms-condition-modal',
  templateUrl: './terms-condition.component.html'
})
export class TermsConditionModule {

  constructor(
    public dialogRef: MatDialogRef<TermsConditionModule>, public dialog: MatDialog) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    const matDialogConfig = new MatDialogConfig()

    matDialogConfig.position = { top: `10px` }
    this.dialogRef.updatePosition(matDialogConfig.position)
  }
}
