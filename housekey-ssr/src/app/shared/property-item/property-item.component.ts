import { Component, OnInit, Input, ViewChild, SimpleChange, Output, EventEmitter } from '@angular/core';
import { SwiperDirective, SwiperConfigInterface, SwiperPaginationInterface } from 'ngx-swiper-wrapper'; 
import { Property, UserData } from '../../app.models';
import { Settings, AppSettings } from '../../app.settings';

import { AppService } from '../../app.service'; 
import { CompareOverviewComponent } from '../compare-overview/compare-overview.component'; 
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoginService } from 'src/app/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-property-item',
  templateUrl: './property-item.component.html',
  styleUrls: ['./property-item.component.scss'] 
})
export class PropertyItemComponent implements OnInit {
  @Input() property: Property;
  @Input() viewType: string = "grid";
  @Input() viewColChanged: boolean = false; 
  @Input() fullWidthPage: boolean = true;   
  @Output() onDeleteClick: EventEmitter<any> = new EventEmitter<any>();
  public column:number = 4;
  cState: string;
  // public address:string; 
  @ViewChild(SwiperDirective) directiveRef: SwiperDirective;
  public config: SwiperConfigInterface = {};
  public user:UserData;
  private pagination: SwiperPaginationInterface = {
    el: '.swiper-pagination',
    clickable: true
  };
  public settings: Settings;
  constructor(public appSettings:AppSettings,public router: Router, public appService:AppService,private snackBar: MatSnackBar,public loginService: LoginService) {
    this.settings = this.appSettings.settings;
  }

  ngOnInit() { 
    this.user = this.loginService.getUser();
  }
  ngAfterViewInit() {
    this.appService.currentState.subscribe(stateName => this.cState = stateName);
    this.initCarousel();
    // this.appService.getAddress(this.property.location.lat, this.property.location.lng).subscribe(data=>{
    //   console.log(data['results'][0]['formatted_address']);
    //   this.address = data['results'][0]['formatted_address'];
    // })
  } 
 
  ngOnChanges(changes: {[propKey: string]: SimpleChange}){  
    if(changes.viewColChanged){
      this.getColumnCount(changes.viewColChanged.currentValue);
      if(!changes.viewColChanged.isFirstChange()){
        if(this.property.gallery.length > 1){     
           this.directiveRef.update();  
        } 
      }
    } 

    for (let propName in changes) {      
      // let changedProp = changes[propName];
      // if (!changedProp.isFirstChange()) {
      //   if(this.property.gallery.length > 1){
      //     this.initCarousel();
      //     this.config.autoHeight = true;       
      //     this.directiveRef.update();  
      //   }       
      // }      
    }  
  }

  public getColumnCount(value){
    if(value == 25){
      this.column = 4;
    }
    else if(value == 33.3){
      this.column = 3;
    }
    else if(value == 50){
      this.column = 2
    }
    else{
      this.column = 1;
    }
  }

  public getStatusBgColor(status){
    switch (status) {
      case 'Sale':
        return '#d32f2f';  
      case 'Lease':
        return '#d32f2f'; 
      case 'Open House':
        return '#d32f2f';
      case 'No Fees':
        return '#d32f2f';
      case 'Hot Offer':
        return '#d32f2f';
      case 'Sold':
        return '#d32f2f';
      default: 
        return '#d32f2f';
    }
  }

  public initCarousel(){
    this.config = {
      slidesPerView: 1,
      spaceBetween: 0,         
      keyboard: false,
      navigation: true,
      pagination: this.pagination,
      grabCursor: true,        
      loop: true,
      preloadImages: false,
      lazy: true,  
      nested: true,
      // autoplay: {
      //   delay: 5000,
      //   disableOnInteraction: false
      // },
      speed: 500,
      effect: "slide"
    }
  }

  public addToCompare(){
    this.appService.addToCompare(this.property, CompareOverviewComponent, (this.settings.rtl) ? 'rtl':'ltr'); 
  }

  public onCompare(){
    return this.appService.Data.compareList.filter(item=>item.id == this.property.id)[0];
  }

  public deleteFavorites(property:Property){
    if(this.loginService.getUser()){
      this.appService.deleteFavorites(property.id).subscribe(res=>{
        if(res.status== 'success'){
          property.favorite = false;
          this.snackBar.open('The property has been removed from favorites', '×', {
            verticalPosition: 'top',
            duration: 3000,
            direction: 'rtl' 
          });
        }
      })
    }else{
      this.router.navigate(['/login']);
    }
  }


  public deleteFavoritesfromProfile(property:Property){
    if(this.loginService.getUser()){
      this.appService.deleteFavorites(property.id).subscribe(res=>{
        if(res.status== 'success'){
          property.favorite = false;
          this.onDeleteClick.emit();
          this.snackBar.open('The property has been removed from favorites', '×', {
            verticalPosition: 'top',
            duration: 3000,
            direction: 'rtl' 
          });
        }
      })
    }else{
      this.router.navigate(['/login']);
    }
  }

  public addToFavorites(property:Property){
    if(this.loginService.getUser()){
      this.appService.addPropToFavorites(property.id).subscribe(res=>{
        if(res.status== 'success'){
          this.snackBar.open('The property has been added to favorites', '×', {
            verticalPosition: 'top',
            duration: 3000,
            direction: 'rtl' 
          });
          property.favorite = true;
        }
      })
    }else{
      this.router.navigate(['/login']);
    }
  }

  public onFavorites(){
    return this.appService.Data.favorites.filter(item=>item.id == this.property.id)[0];
  }
}
