import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AppService } from '../../app.service'; 
import { LabelType, Options } from 'ng5-slider';
import { Router } from '@angular/router'; 
import { LocationC, SearchObj } from 'src/app/app.models';
import {Observable} from 'rxjs';
import { MatOptionSelectionChange } from '@angular/material/core';
import { TranslateService } from '@ngx-translate/core';
import { debounceTime, filter, finalize, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-properties-search',
  templateUrl: './properties-search.component.html',
  styleUrls: ['./properties-search.component.scss']
})
export class PropertiesSearchComponent implements OnInit, AfterViewInit {
  @Input() variant:number = 1;
  @Input() vertical:boolean = false;
  @Input() isHorizontal1:string = "";
  @Input() searchOnBtnClick:boolean = false;
  @Input() removedSearchField:string;
  @Output() onSearchChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() onSearchClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() onResetClick: EventEmitter<any> = new EventEmitter<any>();
  public showMore: boolean = false;
  public isForSale: boolean = true;
  public bedrooms = new Set();
  public bathrooms = [];
  public parkings = [];
  public currentLoc:LocationC;
  public form: FormGroup;
  public searchValues = [];
  public searchText = "";
  public searchoptions: any[] = [];
  public form1: FormGroup;
  public propertyTypes = [];
  public propertyOptionsRes = [];
  public propertyOptionsComm = [];
  public propertyStatuses = [];
  public propertyStyles = [];
  public propertyStyle:any;
  public propertyOpenTimes = [];
  public propertySoldDays = [];
  public propertyActiveTime = [];
  public propertyStatusList = [];
  public basementStyles = [];
  public searchObj:SearchObj;
  public searchObjNew:SearchObj;
  public cities = [];
  public neighborhoods = [];
  public streets = [];
  public features = [];
  public prTypeStatus = "";
  public openHouseDay = "-1";
  public isSoldFlag: boolean = false;
  public isActiveFlag: boolean = false;
  public filteredOptions: Observable<string[]>;
  public slidersRefresh: EventEmitter<void> = new EventEmitter<void>();
  public options: Options = {
    floor: 500,
    ceil: 20000
  };
  public optionsNew: Options = {
    floor: 500,
    ceil: 1000000,
    translate: (value: number, label: LabelType): string => {
      return '$' + value.toLocaleString('en');
    }
  };

  isLoading: boolean;
  value: number = 0;
  highValue: number = 0;
  valuenew: number = 0;
  highValuenew: number = 0;

  constructor(public appService:AppService, public fb: FormBuilder, public router:Router,public translate: TranslateService) { 
    this.currentLoc = {
      id: 1,
      lat: 43.63943345,
      lng: -79.39972759226308};
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if(Object.keys(this.appService.getSearchObject()).length != 0){
        var temp = this.appService.getSearchObject();
        if(temp){
          this.setFilledFilterSearch(temp);
        }
      }else{
      }
    }, 500);
    this.form1.get("myControl").valueChanges
    .pipe(
      filter(text => text.length > 2),
      debounceTime(500),
      tap(() => {
        this.searchoptions = [];
        this.isLoading = true;
      }),
      switchMap(value => this.appService.searchAddress(this.getSearchFiltNew(value))
        .pipe(
          finalize(() => {
            //this.onSearchChange.emit(this.form);
            this.isLoading = false
          }),
        )
      )
    )
    .subscribe(data => {
      if (data.data == undefined) {
        this.searchoptions = [];
      } else {
        if(data.data && data.data.length > 0){
          this.searchValues = [{
              name: 'Addresses',
              data: data.data
            }
          ]
          this.searchoptions = data.data;
        }
      }
    });
    //throw new Error("Method not implemented.");
  }

  ngOnInit() {
    if(this.vertical){
      this.showMore = true;
    };
    this.propertyOptionsRes = this.appService.getPropertyTypesRes();
    this.propertyOptionsComm = this.appService.getPropertyTypesComm();
    this.propertyStatuses = this.appService.getPropertyStatuses();
    this.basementStyles = this.appService.getBasementType();
    this.propertyOpenTimes = this.appService.getPropertyOpenTime();
    this.propertySoldDays = this.appService.getPropertySoldDays();
    this.propertyActiveTime = this.appService.getPropertyActiveTime();
    this.propertyStatusList = this.appService.getPropertyStatusList();
    this.propertyStyles = this.appService.getPropertyStyle();
    this.cities = this.appService.getCities();
    this.neighborhoods = this.appService.getNeighborhoods();
    this.streets = this.appService.getStreets();
    this.features = this.appService.getFeatures();
    this.form1 = this.fb.group({
      myControl: new FormControl()
    })
    this.form = this.fb.group({
      propertyType: null,
      propertyStatus: false,
      typeText: null,
      saleLease: "Sale",
      address: false,
      propertyOption: null,
      propertySoldDays: null,
      basementStyle: null,
      heatSource: null,
      daysOnMarket: null,
      heatType: null,
      airConditioningType: null,
      poolType: null,
      exteriorType: null, 
      price: this.fb.group({
        from: null,
        to: null 
      }),
      city: null,
      zipCode: null,
      neighborhood: null,
      street: null,
      bedrooms: [],
      washrooms: null,
      openHouse: null,
      withSold:false,
      parkingSpaces: "",
      area: this.fb.group({
        from: null,
        to: null 
      }),
      sliderControlGroup: this.fb.group({
        sliderControl: new FormControl([0, 0]),
      }),
      myControl: new FormControl(),
      yearBuilt: this.fb.group({
        from: null,
        to: null 
      }),
      features: this.buildFeatures()
    });
    let self = this;
    setTimeout(() => {
      self.form.controls['propertySoldDays'].setValue('', {emitEvent: false});
    }, 1000);
    this.onSearchChange.emit(this.form);
  }

  addBedrooms(type:string){
    if(this.bedrooms.has(type)){
      this.bedrooms.delete(type);
    }else if(type === 'All'){
      this.bedrooms.clear();
    }else{
      this.bedrooms.add(type);
    }
    this.form.patchValue({
      bedrooms : this.bedrooms  
    })
  }
  
  addBathrooms(type:string){
    if(type == "All"){
      this.bathrooms = [];
      this.form.patchValue({
        washrooms : "0"
      })
    }else{
      this.bathrooms = [];
      this.bathrooms.push(type);
      this.form.patchValue({
        washrooms : this.bathrooms[0]
      })
    }
  }
  addOpenHouse(type:string){
    this.openHouseDay = type;
    this.form.patchValue({
      openHouse : type
    })
  }

  addParkings(type:string){
    this.parkings = [];
    this.parkings.push(type);
    this.form.patchValue({
      parkingSpaces : this.parkings[0]
    })
  }

  changePrType(type:string){
    if(type == 'sale'){
      this.form.patchValue({
        saleLease : "Sale"  
      })
      this.isForSale = true;
    }else if(type == 'rent'){
      this.form.patchValue({
        saleLease : "Lease"  
      })
      this.isForSale = false;
    }
  }

  changePrTypeStatus(type:string){
    this.prTypeStatus = type;
    this.form.patchValue({
      propertyType : type  
    })
  }
  
  public typeChanged(event){
    if(event != "Residential"){
      this.searchObj.propertyType = "residential";
    }else if(event != "Commercial"){
      this.searchObj.propertyType = "commercial";
    }
  }

  setFilledFilterSearch(obj) {
    if(obj){
      for (let i = 0; i < obj.bedRooms.length; i++) {
        this.bedrooms.add(obj.bedRooms[i]);
      }
      if(obj.parkingSpaces){
        this.parkings = [];
        this.parkings.push(obj.parkingSpaces);
      }
      if(obj.propertyType && obj.propertyType != ""){
        this.form.patchValue({
          propertyType : obj.propertyType
        })
        this.prTypeStatus = obj.propertyType;
      }else{
        this.form.patchValue({
          propertyType : "residential"
        })
        this.prTypeStatus = "residential";
      }
      if(obj.washrooms){
        this.bathrooms = [];
        this.bathrooms.push(obj.washrooms);
      }
      this.form.patchValue({
        washrooms:obj.washrooms,
        parkingSpaces:obj.parkingSpaces,
        basementStyle : obj.basement1,
        propertyOption : obj.propertyOption,
        openHouse : obj.openHouse,
        price:{from: obj.listPriceMin,to: obj.listPriceMax}
      })
      this.openHouseDay = obj.openHouse;
      if(obj.approxSquareFootageMin){
        this.value = parseInt(obj.approxSquareFootageMin);
      }
      if(obj.approxSquareFootageMax){
        this.highValue = parseInt(obj.approxSquareFootageMax);
      }
      if(obj.listPriceMin){
        this.valuenew = parseInt(obj.listPriceMin);

      }
      if(obj.listPriceMax){
        this.highValuenew = parseInt(obj.listPriceMax);
      }
      this.form1.controls['myControl'].setValue(obj.address);
      this.form.controls['daysOnMarket'].setValue(obj.daysOnMarket);
      this.form.controls['propertySoldDays'].setValue(obj.propertySoldDays);
      if(obj.saleLease == 'Lease'){
        this.isForSale = false;
      }else if(obj.saleLease == 'Sale'){
        this.isForSale = true;
      }else{
        //this.form.patchValue({propertyType : "Lease"})
      }

      this.form.get('sliderControlGroup').patchValue({'sliderControl':[this.valuenew,this.highValuenew]});
    }
  }

  private getSearchFilt(text:string){
    this.searchText = text;
    this.searchObjNew = this.appService.getSearchObject();
    this.searchObjNew.address = text
    this.searchObjNew.bedRooms = Array.from(this.searchObjNew.bedRooms);
    return this.searchObjNew;
  }

  private getSearchFiltNew(text:string){
    return {"currentPage":1,"searchText":text}
  }

  modelChanged(newObj) {
    // do something with new value
  }
  
  public buildFeatures() {
    const arr = this.features.map(feature => { 
      return this.fb.group({
        id: feature.id,
        name: feature.name,
        selected: feature.selected
      });
    })   
    return this.fb.array(arr);
  }
  
  ngOnChanges(){ 
    if(this.removedSearchField){ 
      if(this.removedSearchField.indexOf(".") > -1){
        let arr = this.removedSearchField.split(".");
        this.form.controls[arr[0]]['controls'][arr[1]].reset();
      }else if(this.removedSearchField.indexOf(",") > -1){        
        let arr = this.removedSearchField.split(","); 
        this.form.controls[arr[0]]['controls'][arr[1]]['controls']['selected'].setValue(false);  
      }else{
        this.form.controls[this.removedSearchField].reset();
      }  
    }  
  }

  public reset(){ 
    this.form.get('sliderControlGroup').patchValue({'sliderControl':[0,0]});
    this.form1.controls['myControl'].setValue("");
    this.parkings = [];
    this.bathrooms = [];
    this.prTypeStatus = "residential";
    this.bedrooms.clear();
    this.isForSale = true;
    this.form.reset({ 
      propertyType: "residential",
      saleLease:"Sale",
      propertyStatus: false,
      basementStyle: null,
      propertyOption: null,
      heatSource: null,
      heatType: null,
      airConditioningType: null,
      poolType: null,
      exteriorType: null,
      price: this.fb.group({
        from: null,
        to: null 
      }),
      city: null,
      parkingSpaces: null,
      withSold:false,
      zipCode: null,
      neighborhood: null,
      street: null,
      bedrooms: [],
      washrooms: null,
      openHouse: "-1",
      area: this.fb.group({
        from: null,
        to: null 
      }),
      sliderControlGroup: this.fb.group({
        sliderControl: new FormControl([0, 0]),
      }),
      yearBuilt: {
        from: null,
        to: null 
      },       
      features: this.features    
    }); 
    this.isSoldFlag = false;
    this.isActiveFlag = false;
    this.appService.clearSearchFields();
    this.onResetClick.emit(this.form);
  }

  public search(){
    this.onSearchClick.emit(this.form);
  }

  public somethingChanged(){
  }

  public onSelectCity(){
    this.form.controls['neighborhood'].setValue(null, {emitEvent: false});
    this.form.controls['street'].setValue(null, {emitEvent: false});
  }

  public onSelectNeighborhood(){
    this.form.controls['street'].setValue(null, {emitEvent: false});
  }

  public getAppearance(){
    return (this.variant != 3) ? 'outline' : '';
  }

  public getFloatLabel(){
    return (this.variant == 1) ? 'always' : '';
  }

  optionSelected(event: MatOptionSelectionChange) {
    if(event.source.value){
      this.form1.patchValue({
        myControl: event.source.value.address
      })
      this.form.patchValue({
        address: event.source.value.address
      })
      if(event.source.value.longitude && event.source.value.latitude){
        this.currentLoc.lat = event.source.value.latitude;
        this.currentLoc.lng = event.source.value.longitude;
        this.appService.setLocation(this.currentLoc);
      }
    }
  }
}
