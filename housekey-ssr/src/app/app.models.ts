export class Property {
    constructor(public id: number,
                public title: string,
                public style: string,
                public desc: string,
                public propertyType: string,
                public propertyStatus: string[], 
                public city: string,
                public mls: string,
                public maintenance: number,
                public municipality: string,
                public zipCode: string[],
                public neighborhood: string[],
                public street: string[],
                public contractDate: string,
                public daysOnMarket: number,
                public soldDate: number,
                public location: Location,
                public locationSearch: string,
                public formattedAddress: string,
                public features: string[],
                public featured: boolean,
                public favorite: boolean,
                public priceDollar: Price,
                public priceEuro: Price,
                public bedrooms: number,
                public bathrooms: number,
                public garages: number,
                public garageSpaces: number,
                public listPrice: number,
                public area: Area,
                public yearBuilt: number,
                public ratingsCount: number,
                public ratingsValue: number,
                public additionalFeatures: AdditionalFeature[],
                public gallery: Gallery[],
                public imageGallery: GalleryNew[],
                public propertyImage: string[],
                public propertyImage1: string,
                public imageNew: string,
                public plans: Plan[],
                public videos: Video[],
                public published: string,
                public lastUpdate: string,
                public virtualTourURL: string,
                public views: number,
                public saleLease: string,
                public address: string,
                public propertyOption: string,
                public approxSquareFootage: number,
                public approxSquareFootageMax: number,
                public washrooms: number,
                public parkingSpaces: number,
                public possessionDate: number,
                public type: string,
                public event: string,
                public listingEntryDate: number,
                public terminatedDate: number,
                public difference_In_ListingDays: number,
                public openHouseUpdatedTimestamp: number,
                public schools: School,
                public latitude: number,
                public longitude: number,
                public nearby: NearByProperties,
                public locationsData:Property[]
                ){ }
}

// export class Property {
//     public id: number;
//     public title: string; 
//     public desc: string;
//     public propertyType: string;
//     public propertyStatus: string[];
//     public city: string;
//     public zipCode: string;
//     public neighborhood: string[];
//     public street: string[];
//     public location: Location;
//     public formattedAddress: string;
//     public features: string[];
//     public featured: boolean;
//     public priceDollar: Price;
//     public priceEuro: Price;
//     public bedrooms: number;
//     public bathrooms: number;
//     public garages: number;
//     public area: Area;
//     public yearBuilt: number;
//     public ratingsCount: number;
//     public ratingsValue: number;
//     public additionalFeatures: AdditionalFeature[];
//     public gallery: Gallery[];
//     public plans: Plan[];
//     public videos: Video[];
//     public published: string;
//     public lastUpdate: string;
//     public views: number
// }


export class Area {
    constructor(public id: number, 
                public value: number,
                public unit: string){ }
}

export class SearchObj {
    constructor(public currentPage: number, 
                public propertyType: string,
                public saleLease: string,
                public propertyOption: string,
                public parkingSpaces: string,
                public basement1: string,
                public washrooms: string,
                public approxSquareFootageMin: string,
                public approxSquareFootageMax: string,
                public daysOnMarket: string,
                public soldDays: string,
                public listPriceMin: string,
                public listPriceMax: string,
                public bedRooms: string[],
                public address: string,
                public latitude: string,
                public longitude: string,
                public openHouse: string,
                public withSold: boolean,
                public radius: string,
                public userId: string){ }
}

export class AdditionalFeature {
    constructor(public id: number, 
                public name: string,
                public value: string){ }
}

export class Location {
    constructor(public id: number, 
                public lat: number,
                public lng: number){ }
}

export class LocationC {
    constructor(public id: number, 
                public lat: number,
                public lng: number){ }
}
export class Locs {
    constructor(public id: number, 
                public lat: number,
                public lng: number){ }
}
export class School {
    constructor(public id: number, 
                public business_status: string,
                public formatted_address: string,
                public geometry: Geometry,
                public icon: string,
                public name: string,
                public photos: string,
                public place_id: string,
                public plus_code: number,
                public rating: number,
                public reference: string,
                public types: string,
                public results: Results[],
                public user_ratings_total: number){ }
}
export class Results {
    constructor(public id: number, 
                public business_status: string,
                public formatted_address: string,
                public geometry: Geometry,
                public icon: string,
                public name: string,
                public photos: string,
                public place_id: string,
                public plus_code: number,
                public rating: number,
                public reference: string,
                public types: string,
                public user_ratings_total: number){ }
}
export class Geometry {
    public location: Location;
    public viewport: number;
}
export class NearByProperties {
    public leaseProperties: Property[];
    public soldProperties: Property[];
}

export class Price {
    public sale: number;
    public rent: number;
}


export class Gallery {
    constructor(public id: number, 
                public small: string,
                public medium: string,
                public big: string){ }
}
export class GalleryNew {
    constructor(public id: number, 
                public allImagesPath: string,
                public fileName: string,
                public mls: string,
                public path: string){ }
}
export class Plan {
    constructor(public id: number, 
                public name: string,
                public desc: string,
                public area: Area,
                public rooms: number,
                public baths: number,
                public image: string){ }
}

export class Video {
    constructor(public id: number, 
                public name: string,
                public link: string){ }
}

export class Pagination {
    constructor(public page: number,
                public perPage: number,
                public prePage: number,
                public nextPage: number,
                public total: number,
                public totalPages: number){ }
}

export class UserCred {
    constructor(public email: string,
                public password: string){ }
}

export class UserData {
    constructor(public token: string,
                public userVM: UserVM){ }
}

export class UserVM {
    constructor(public createdDate: string,
                public email: string,
                public firstname: string,
                public imageName: string,
                public lastname: string,
                public modifiedDate: string,
                public role: string,
                public id: number,
                public active:boolean,
                public phoneNumber: string){ }
}
