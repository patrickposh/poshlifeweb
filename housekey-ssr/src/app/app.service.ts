import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Property, LocationC, UserCred, SearchObj } from './app.models';
import { AppSettings } from './app.settings';
import { isPlatformBrowser } from '@angular/common';
import { LoginService } from './login.service';

export class Data {
  constructor(public properties: Property[],
              public compareList: Property[],
              public favorites: Property[],
              public locations: Location[]) {
                
               }
}

@Injectable({
  providedIn: 'root'
})
export class AppService {
  public Data = new Data(
    [], // properties
    [], // compareList
    [], // favorites
    []  // locations
  )
  public urlNew = "https://property.mindnerves.com/";
  public userImageURL = this.urlNew + "api/user/download-image/";
  public propertyImageURl = this.urlNew + "api/property/download-image/";
  public propertyMapImageURl = this.urlNew + "api/property/map-image/";
  public blogImageURl = this.urlNew + "api/blog/download-blogImage/";

  public url = "../../assets/data/";
  public apiKey = 'AIzaSyA1rF9bttCxRmsNdZYjW7FzIoyrul5jb-s';
  private searchObject: SearchObj;
  private subData: any[];
  public userLoc: LocationC;
  private messageSource = new BehaviorSubject('default message');
  private stateName = new BehaviorSubject('');
  private profileImage = new BehaviorSubject('assets/images/others/user.jpg');
  private userRole = new BehaviorSubject('');
  currentMessage = this.messageSource.asObservable();
  currentState = this.stateName.asObservable();
  currentImage = this.profileImage.asObservable();
  currentRole = this.userRole.asObservable();
  constructor(public http:HttpClient, 
              private bottomSheet: MatBottomSheet, 
              private snackBar: MatSnackBar,
              public appSettings:AppSettings, public loginService: LoginService,
              @Inject(PLATFORM_ID) private platformId: Object) { 
                this.userLoc = {
                  id:1,
                  lat:0,
                  lng:0};
                this.searchObject = {currentPage:1,
                  propertyType:"",
                  address:"",
                  saleLease:"Sale",
                  propertyOption:"",
                  parkingSpaces:"",
                  basement1:"",
                  washrooms:"",
                  approxSquareFootageMin:"",
                  approxSquareFootageMax:"",
                  daysOnMarket:"",
                  soldDays:"",
                  listPriceMin:"",
                  listPriceMax:"",
                  bedRooms:[],
                  latitude:"",
                  longitude:"",
                  openHouse:"",
                  withSold:false,
                  radius:"",
                  userId:""};
              }
    
  public getSearchObject(){
    if(localStorage.getItem("searchFields")){
      this.searchObject = JSON.parse(localStorage.getItem("searchFields"));
    }
    return this.searchObject;
  }
  public getSearchObjectForAddressSearch(){
    return {currentPage:1,
      propertyType:"",
      address:"",
      saleLease:"",
      propertyOption:"",
      rooms:"",
      parkingSpaces:"",
      basement1:"",
      washrooms:"",
      streetName:"",
      approxSquareFootageMin:"",
      approxSquareFootageMax:"",
      pIN:"",
      daysOnMarket:"",
      soldDays:"",
      listPriceMin:"",
      listPriceMax:"",
      bedRooms:[],
      latitude:"",
      longitude:"",
      openHouse:"",
      withSold:false,
      radius:"",
      userId:""};
  }

  public getRadius(zoomlevel:number){
    var radius = 5
    switch (zoomlevel) {
      case 8:
        radius =  150;    
        break;
      case 9:
        radius =  120;             
        break;
      case 10:
        radius =  100;    
        break;
      case 11:
        radius =  30;             
        break;
      case 12:
        radius =  10;             
        break;
      case 13:
        radius =  8;             
        break;
      case 14:
        radius =  7;             
        break;
      case 15:
        radius =  6;             
        break;
      case 16:
        radius =  5;             
        break;
      case 17:
        radius =  4;             
        break;
      case 18:
        radius =  3;                          
        break;
      default:
        break;
    }
    return radius;
  }

  public setSearchAddress(text:string){
    this.searchObject.address = text;
  }

  public setSubData(data:any[]){
    this.subData = data;
  }

  public getSubData(){
    return this.subData;
  }

  public setSearchPropertyType(text:string){
    //this.searchObject.address = text;
  }

  public clearSearchFields(){
    this.searchObject = {currentPage:0,
      propertyType:"residential",
      address:"",
      saleLease:"Sale",
      propertyOption:"",
      parkingSpaces:"",
      basement1:"",
      washrooms:"",
      approxSquareFootageMin:"",
      approxSquareFootageMax:"",
      daysOnMarket:"",
      soldDays:"",
      listPriceMin:"",
      listPriceMax:"",
      bedRooms:[],
      latitude:"",
      longitude:"",
      openHouse:"",
      withSold:false,
      radius:"",
      userId:""};
      localStorage.setItem("searchFields", JSON.stringify(this.searchObject));
  }

  changeMessage(message: string) {
    this.messageSource.next(message)
  }
  
  changeState(stateName: string) {
    this.stateName.next(stateName)
  }

  changeProfileImage(profileImage: string) {
    if(!profileImage || profileImage == '' || profileImage == null){
      this.profileImage.next("assets/images/others/user.jpg")
    }else{
      this.profileImage.next(this.userImageURL + profileImage)
    }
    
  }

  changeUserRole(userRole: string) {
    this.userRole.next(userRole)
  }

  public setSearchObject(obj:any){
    if(obj.propertyType != undefined && obj.propertyType != null){
      if(obj.propertyType != ""){
        this.searchObject.propertyType = obj.propertyType;
      }
    }
    if(obj.bedrooms){
      this.searchObject.bedRooms = [];
      var self = this;
      obj.bedrooms.forEach(function(item){
        self.searchObject.bedRooms.push(item.toString())
      });
    }
    if(obj.address){
      this.searchObject.address = obj.address;
    }
    if(obj.openHouse){
      this.searchObject.openHouse = obj.openHouse;
    }
    if(obj.propertyOption){
      this.searchObject.propertyOption = obj.propertyOption;
    }
    if(obj.saleLease != ""){
      this.searchObject.saleLease = obj.saleLease;
    }else{
      this.searchObject.saleLease = "Sale";
    }
    if(obj.parkingSpaces != undefined && obj.parkingSpaces != null && !Number.isNaN(obj.parkingSpaces)){
      this.searchObject.parkingSpaces = obj.parkingSpaces;
    }
    if(obj.basementStyle){
      this.searchObject.basement1 = obj.basementStyle;
    }
    if(obj.washrooms != undefined && obj.washrooms != null && !Number.isNaN(obj.washrooms)){
      this.searchObject.washrooms = obj.washrooms;
    }
    if(obj.daysOnMarket){
      this.searchObject.daysOnMarket = obj.daysOnMarket;
    }else{
      this.searchObject.daysOnMarket = "";
    }
    if(obj.withSold){
      this.searchObject.withSold = obj.withSold;
    }else{
      this.searchObject.withSold = false;
    }
    if(obj.price.from){
      this.searchObject.listPriceMin = obj.price.from;
    }
    if(obj.price.to){
      this.searchObject.listPriceMax = obj.price.to;
    }
    if(obj.sliderControlGroup){
      if(obj.sliderControlGroup.sliderControl[0] != 500 && obj.sliderControlGroup.sliderControl[0] != 0 && obj.sliderControlGroup.sliderControl[1] != 0){
        this.searchObject.approxSquareFootageMin = obj.sliderControlGroup.sliderControl[0];
      }
      if(obj.sliderControlGroup.sliderControl[1] != 500 && obj.sliderControlGroup.sliderControl[1] != 0){
        this.searchObject.approxSquareFootageMax = obj.sliderControlGroup.sliderControl[1];
      }
    }
    if(obj.propertySoldDays){
      if(obj.propertySoldDays == -1){
        this.searchObject.soldDays = "";
      }else{
        this.searchObject.soldDays = obj.propertySoldDays;
      }
    }else{
      this.searchObject.soldDays = "";
    }
    // this.searchObject.latitude = "";
    // this.searchObject.longitude = "";
    localStorage.setItem("searchFields", JSON.stringify(this.searchObject));
  }
  
  public getProperties(): Observable<Property[]>{
    return this.http.get<Property[]>(this.url + 'properties.json');
  }

  public getPropertiesnew(obj:any): Observable<any>{
    return this.http.post(this.urlNew + 'homepage/recommendlist',obj);
  }

  public getPropertynew(id:string): Observable<any>{
    return this.http.get(this.urlNew + 'api/property/'+id);
  }
  public deleteFavorites(id:number): Observable<any>{
    return this.http.delete(this.urlNew + 'api/favorite/'+this.loginService.getUser().userVM.id+"/"+id);
  }

  public addPropToFavorites(id:number): Observable<any>{
    var obj = {
      "favoriteId":id,
      "userId":this.loginService.getUser().userVM.id,
      "createdBy":"admin",
      "createdDate":"",
      "modifiedDate":""
    }
    return this.http.post(this.urlNew + 'api/favorite/',obj);
  }
  public getFavorites(): Observable<any>{
    if(!this.loginService.getUser()){
      return null
    }
    return this.http.get(this.urlNew + 'api/favorite/user/'+this.loginService.getUser().userVM.id);
  }

  public getLogin(user:UserCred): Observable<any>{
    return this.http.post(this.urlNew,{
      "email":"kshirsagarvinayak97@gmail.com",
      "password":"password"
  });
  }

  public searchProperty(obj:SearchObj): Observable<any>{
    if(this.loginService.getUser()){
      if(this.loginService.getUser().userVM){
        obj.userId = this.loginService.getUser().userVM.id.toString();
      }
    }
    return this.http.post(this.urlNew + "api/property/searchAttributes",obj);
  }

  public setLocation(loc:LocationC){
    this.searchObject.latitude =  loc.lat.toString();
    this.searchObject.longitude = loc.lng.toString();
    this.userLoc = loc;
    localStorage.setItem("searchFields", JSON.stringify(this.searchObject));
    localStorage.setItem('location',JSON.stringify(loc));
  }

  public getLocation(){
    var temp = localStorage.getItem('location');
    if(temp){
      var loc:Location = JSON.parse(temp);
      return loc
    }
    return null;
  }

  public searchPropertyMap(obj:SearchObj): Observable<any>{
    if(this.loginService.getUser()){
      if(this.loginService.getUser().userVM){
        obj.userId = this.loginService.getUser().userVM.id.toString();
      }
    }
    return this.http.post(this.urlNew + "api/property/mapSearch",obj);
  }

  public searchAddress(obj:any): Observable<any>{
    return this.http.post(this.urlNew + "api/property/search",obj);
  }

  public preAprovedMortgage(obj:any): Observable<any>{
    var temp = "Bearer " + this.loginService.getUser().token;
    var headers = { 'Authorization': temp, 'Content-Type': 'application/json' }
    return this.http.post(this.urlNew + 'api/mortgage/',obj, { headers });
  }

  public updateSchedule(obj:any): Observable<any>{
    var temp = "Bearer " + this.loginService.getUser().token;
    var headers = { 'Authorization': temp, 'Content-Type': 'application/json' }
    return this.http.put(this.urlNew + 'api/scheduleVisit/broker',obj, { headers });
  }

  public getSchedule(id:string): Observable<any>{
    var temp = "Bearer " + this.loginService.getUser().token;
    var headers = { 'Authorization': temp, 'Content-Type': 'application/json' }
    return this.http.get(this.urlNew + 'api/scheduleVisit/'+id,{ headers });
  }
  
  public getAllUsers(): Observable<any>{ 
    var temp = "Bearer " + this.loginService.getUser().token;
    var headers = { 'Authorization': temp, 'Content-Type': 'application/json' }
    return this.http.get(this.urlNew + 'api/user/findAllUsers',{ headers });
  }

  public getAllBlogs(): Observable<any>{
    var temp = "Bearer " + this.loginService.getUser().token;
    var headers = { 'Authorization': temp, 'Content-Type': 'application/json' }
    return this.http.get(this.urlNew + 'api/blog/',{ headers });
  }

  public getBlogDetail(blogId): Observable<any>{
      var temp = "Bearer " + this.loginService.getUser().token;
      var headers = { 'Authorization': temp, 'Content-Type': 'application/json' }
      return this.http.get(this.urlNew + 'api/blog/'+blogId,{ headers });
  }

  public uploadBlogPicture(obj:any,token:string): Observable<any>{
    let getHeaders: HttpHeaders = new HttpHeaders({
      'Authorization':token
    });
    var imageData = new FormData();
    imageData.append("file",obj);
    return this.http.post(this.urlNew + 'api/blog/blogFileUploader',imageData, {headers: getHeaders});
  }
  
  public getAllSchduledVisits(): Observable<any>{ 
    var temp = "Bearer " + this.loginService.getUser().token;
    var headers = { 'Authorization': temp, 'Content-Type': 'application/json' }
    return this.http.get(this.urlNew + 'api/scheduleVisit/',{ headers });
  }

  public searchSchools(add:string): Observable<any>{
    var temp = {
      "address":""
    }
    return this.http.post(this.urlNew + "homepage/schools/"+add,temp);
  }
  
  public searchSchoolsByLocation(obj:any): Observable<any>{
    return this.http.post(this.urlNew + "homepage/schools",obj);
  }

  public searchPropertyOptions(obj:any): Observable<any>{
    return this.http.post(this.urlNew + "api/property/searchAddress",obj);
  }

  public subscribeToNewsLatter(obj:any): Observable<any>{
    return this.http.post(this.urlNew + 'api/subscription/',obj);
  }

  public scheduleVisit(obj:any): Observable<any>{
    var temp = "Bearer " + this.loginService.getUser().token;
    var headers = { 'Authorization': temp, 'Content-Type': 'application/json' }
    return this.http.post(this.urlNew + 'api/scheduleVisit/',obj, { headers });
  }

  public addBlog(obj:any,token:string): Observable<any>{
    let getHeaders: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization':token
    });
    return this.http.post(this.urlNew + 'api/blog/',obj, {headers: getHeaders});
  }

  public updateRateOfInterest(obj:any,token:string): Observable<any>{
    let getHeaders: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization':token
    });
    return this.http.put(this.urlNew + 'api/tax/',obj, {headers: getHeaders});
  }

  public getRateOfInterest(key:string): Observable<any>{
    return this.http.get(this.urlNew + 'api/tax/'+key);
  }

  public getLocations(): Observable<Location[]>{
    return this.http.get<Location[]>(this.url + 'locations.json');
  }

  public getAddress(lat = 40.714224, lng = -73.961452){ 
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&key='+this.apiKey);
  }

  public getLatLng(address){ 
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?key='+this.apiKey+'&address='+address);
  }

  public getFullAddress(lat = 40.714224, lng = -73.961452){ 
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&key='+this.apiKey).subscribe(data =>{ 
      return data['results'][0]['formatted_address'];
    });
  }

  public addToCompare(property:Property, component, direction){ 
    if(!this.Data.compareList.filter(item=>item.id == property.id)[0]){
      this.Data.compareList.push(property);
      this.bottomSheet.open(component, {
        direction: direction
      }).afterDismissed().subscribe(isRedirect=>{  
        if(isRedirect){
          if (isPlatformBrowser(this.platformId)) {
            window.scrollTo(0,0);
          }
        }        
      }); 
    } 
  }

  public addToFavorites(property:Property, direction){
    if(!this.Data.favorites.filter(item=>item.id == property.id)[0]){
      this.Data.favorites.push(property);
      this.snackBar.open('The property "' + property.title + '" has been added to favorites.', '×', {
        verticalPosition: 'top',
        duration: 3000,
        direction: direction 
      });  
    }    
  }

  public getPropertyTypesRes(){
    return [ 
      { id: 1, name: 'Condo Apt' },
      { id: 2, name: 'Condo Townhouse' },
      { id: 3, name: 'Townhouse' },
      { id: 4, name: 'Detached' },
      { id: 5, name: 'Semi-Detached' },
      { id: 6, name: 'Vacant Land' }
    ]
  }
  public getPropertyTypesComm(){
    return [ 
      { id: 1, name: 'Business' },
      { id: 2, name: 'Industrial' },
      { id: 3, name: 'Office' },
      { id: 4, name: 'All' },

    ]
  }

  public getPropertyStyle(){
    return [ 
      { id: 1, name: 'Any' },
      { id: 2, name: '1 1/2 Story' },
      { id: 3, name: '2-Storey' },
      { id: 4, name: '2 1/2 Story' },
      { id: 5, name: '3-Story' },
      { id: 6, name: 'Backsplit Styles' },
      { id: 7, name: 'Bangalow Styles' },
      { id: 8, name: 'Frontsplit' },
      { id: 9, name: 'Multilevel' },
      { id: 10, name: 'Other' },
      { id: 11, name: 'Sidesplit Styles' }
    ]
  }

  public getPropertyOpenTime(){
    return [ 
      { id: 1, name: '30' },
      { id: 2, name: '100' },
      { id: 3, name: '150' },
      { id: 4, name: '200' }
    ]
  }
  public getPropertyActiveTime(){
    return [ 
      { id: 1, name: '1 days', value: '1' },
      { id: 2, name: '7 days', value: '7' },
      { id: 3, name: '30 days', value: '30' },
      { id: 4, name: '60 days', value: '60' },
      { id: 5, name: '90 days', value: '90' },
      { id: 5, name: 'Any', value: '-1' }
    ]
  }

  public getPropertySoldDays(){
    return [ 
      { id: 1, name: '1 days', value: '1' },
      { id: 2, name: '7 days', value: '7' },
      { id: 3, name: '30 days', value: '30' },
      { id: 4, name: '60 days', value: '60' },
      { id: 5, name: '90 days', value: '90' },
      { id: 5, name: 'Any', value: '-1' }
    ]
  }

  public getBasementType(){
    return [ 
      { id: 1, name: 'None' },
      { id: 2, name: 'Other' },
      { id: 3, name: 'Unfinished' },
      { id: 4, name: 'W/O' },
      { id: 5, name: 'Fin W/O' },
      { id: 6, name: 'Finished' },
      { id: 7, name: 'Apartment' },
      { id: 8, name: 'Part Fin' },
      { id: 9, name: 'Full' },
      { id: 10, name: 'Crawl Space' },
      { id: 11, name: 'Half' },
      { id: 12, name: 'Part Bsmt' },
      { id: 13, name: 'Walk-Up' },
      { id: 14, name: 'Sep Entrance' }
    ]
  }
  
  public getPropertyStatuses(){
    return [ 
      { id: 1, name: 'For Sale' },
      { id: 2, name: 'For Rent' },
      { id: 3, name: 'Open House' },
      { id: 4, name: 'No Fees' },
      { id: 5, name: 'Hot Offer' },
      { id: 6, name: 'Sold' }
    ]
  }

  public getPropertyStatusList(){
    return [ 
      { id: 1, name: 'Active' , value: false },
      { id: 2, name: 'Sold' , value: true }
    ]
  }

  public getCities(){
    return [ 
      { id: 1, name: 'New York' },
      { id: 2, name: 'Chicago' },
      { id: 3, name: 'Los Angeles' },
      { id: 4, name: 'Seattle' } 
    ]
  }

  public getNeighborhoods(){
    return [      
      { id: 1, name: 'Astoria', cityId: 1 },
      { id: 2, name: 'Midtown', cityId: 1 },
      { id: 3, name: 'Chinatown', cityId: 1 }, 
      { id: 4, name: 'Austin', cityId: 2 },
      { id: 5, name: 'Englewood', cityId: 2 },
      { id: 6, name: 'Riverdale', cityId: 2 },      
      { id: 7, name: 'Hollywood', cityId: 3 },
      { id: 8, name: 'Sherman Oaks', cityId: 3 },
      { id: 9, name: 'Highland Park', cityId: 3 },
      { id: 10, name: 'Belltown', cityId: 4 },
      { id: 11, name: 'Queen Anne', cityId: 4 },
      { id: 12, name: 'Green Lake', cityId: 4 }      
    ]
  }

  public getStreets(){
    return [      
      { id: 1, name: 'Astoria Street #1', cityId: 1, neighborhoodId: 1},
      { id: 2, name: 'Astoria Street #2', cityId: 1, neighborhoodId: 1},
      { id: 3, name: 'Midtown Street #1', cityId: 1, neighborhoodId: 2 },
      { id: 4, name: 'Midtown Street #2', cityId: 1, neighborhoodId: 2 },
      { id: 5, name: 'Chinatown Street #1', cityId: 1, neighborhoodId: 3 }, 
      { id: 6, name: 'Chinatown Street #2', cityId: 1, neighborhoodId: 3 },
      { id: 7, name: 'Austin Street #1', cityId: 2, neighborhoodId: 4 },
      { id: 8, name: 'Austin Street #2', cityId: 2, neighborhoodId: 4 },
      { id: 9, name: 'Englewood Street #1', cityId: 2, neighborhoodId: 5 },
      { id: 10, name: 'Englewood Street #2', cityId: 2, neighborhoodId: 5 },
      { id: 11, name: 'Riverdale Street #1', cityId: 2, neighborhoodId: 6 }, 
      { id: 12, name: 'Riverdale Street #2', cityId: 2, neighborhoodId: 6 },
      { id: 13, name: 'Hollywood Street #1', cityId: 3, neighborhoodId: 7 },
      { id: 14, name: 'Hollywood Street #2', cityId: 3, neighborhoodId: 7 },
      { id: 15, name: 'Sherman Oaks Street #1', cityId: 3, neighborhoodId: 8 },
      { id: 16, name: 'Sherman Oaks Street #2', cityId: 3, neighborhoodId: 8 },
      { id: 17, name: 'Highland Park Street #1', cityId: 3, neighborhoodId: 9 },
      { id: 18, name: 'Highland Park Street #2', cityId: 3, neighborhoodId: 9 },
      { id: 19, name: 'Belltown Street #1', cityId: 4, neighborhoodId: 10 },
      { id: 20, name: 'Belltown Street #2', cityId: 4, neighborhoodId: 10 },
      { id: 21, name: 'Queen Anne Street #1', cityId: 4, neighborhoodId: 11 },
      { id: 22, name: 'Queen Anne Street #2', cityId: 4, neighborhoodId: 11 },
      { id: 23, name: 'Green Lake Street #1', cityId: 4, neighborhoodId: 12 },
      { id: 24, name: 'Green Lake Street #2', cityId: 4, neighborhoodId: 12 }      
    ]
  }

  public getFeatures(){
    return [ 
      { id: 1, name: 'Barbeque', selected: false },
      { id: 2, name: 'Dryer', selected: false },
      { id: 3, name: 'Microwave', selected: false }, 
      { id: 4, name: 'Refrigerator', selected: false },
      { id: 5, name: 'TV Cable', selected: false },
      { id: 6, name: 'Sauna', selected: false },
      { id: 7, name: 'WiFi', selected: false },
      { id: 8, name: 'Fireplace', selected: false },
      { id: 9, name: 'Gym', selected: false },
      { id: 10, name: 'AC', selected: false },
      { id: 11, name: 'Pool', selected: false }
    ]
  }


  public getHomeCarouselSlides(){
    return this.http.get<any[]>(this.url + 'slides.json');
  }


  public filterData(data, params: any, sort?, page?, perPage?){ 
    if(params){

      if(params.propertyType){
        
        //data = data.filter(property => property.propertyType == params.propertyType.name)
      }

      if(params.propertyStatus && params.propertyStatus.length){       
        let statuses = [];
        params.propertyStatus.forEach(status => { statuses.push(status.name) });           
        let properties = [];
        data.filter(property =>
          property.propertyStatus.forEach(status => {             
            if(statuses.indexOf(status) > -1){                 
              if(!properties.includes(property)){
                properties.push(property);
              }                
            }
          })
        );
        data = properties;
      }

      if(params.price){
        // if(this.appSettings.settings.currency == 'USD'){          
        //   if(params.price.from){
        //     data = data.filter(property => {
        //       if(property.priceDollar.sale && property.priceDollar.sale >= params.price.from ){
        //         return true;
        //       }
        //       if(property.priceDollar.rent && property.priceDollar.rent >= params.price.from ){
        //         return true;
        //       } 
        //       return false;
        //     });
        //   }
        //   if(params.price.to){
        //     data = data.filter(property => {
        //       if(property.priceDollar.sale && property.priceDollar.sale <= params.price.to){
        //         return true;
        //       }
        //       if(property.priceDollar.rent && property.priceDollar.rent <= params.price.to){
        //         return true;
        //       } 
        //       return false;
        //     });          
        //   }
        // }
        // if(this.appSettings.settings.currency == 'EUR'){
        //   if(params.price.from){
        //     data = data.filter(property => {
        //       if(property.priceEuro.sale && property.priceEuro.sale >= params.price.from ){
        //         return true;
        //       }
        //       if(property.priceEuro.rent && property.priceEuro.rent >= params.price.from ){
        //         return true;
        //       } 
        //       return false;
        //     });

        //   }
        //   if(params.price.to){
        //     data = data.filter(property => {
        //       if(property.priceEuro.sale && property.priceEuro.sale <= params.price.to){
        //         return true;
        //       }
        //       if(property.priceEuro.rent && property.priceEuro.rent <= params.price.to){
        //         return true;
        //       } 
        //       return false;
        //     });
        //   }
        // }        
      }  

      if(params.city){
        data = data.filter(property => property.city == params.city.name)
      }

      if(params.zipCode){
        data = data.filter(property => property.zipCode == params.zipCode)
      }

      if(params.street && params.street.length){       
        let streets = [];
        params.street.forEach(item => { streets.push(item.name) });           
        let properties = [];
        data.filter(property =>
          property.street.forEach(item => {             
            if(streets.indexOf(item) > -1){                 
              if(!properties.includes(property)){
                properties.push(property);
              }                
            }
          })
        );
        data = properties;
      }
      
      if(params.bathrooms){
        if(params.bathrooms.from){
          data = data.filter(property => property.bathrooms >= params.bathrooms.from)
        }
        if(params.bathrooms.to){
          data = data.filter(property => property.bathrooms <= params.bathrooms.to)
        }
      } 

      if(params.parkingSpaces){
        if(params.parkingSpaces){
          data = data.filter(property => property.parkingSpaces >= params.parkingSpaces)
        }
      } 

      if(params.area){
        if(params.area.from){
          data = data.filter(property => property.area.value >= params.area.from)
        }
        if(params.area.to){
          data = data.filter(property => property.area.value <= params.area.to)
        }
      } 

      if(params.yearBuilt){
        if(params.yearBuilt.from){
          data = data.filter(property => property.yearBuilt >= params.yearBuilt.from)
        }
        if(params.yearBuilt.to){
          data = data.filter(property => property.yearBuilt <= params.yearBuilt.to)
        }
      }

      if(params.features){       
        let arr = [];
        params.features.forEach(feature => { 
          if(feature.selected)
            arr.push(feature.name);
        });  
        if(arr.length > 0){
          let properties = [];
          data.filter(property =>
            property.features.forEach(feature => {             
              if(arr.indexOf(feature) > -1){                 
                if(!properties.includes(property)){
                  properties.push(property);
                }                
              }
            })
          );
          data = properties;
        }         
      }
    }
    //for show more properties mock data 
    for (var index = 0; index < 2; index++) {
      data = data.concat(data);        
    }     
     
    this.sortData(sort, data);
    return this.paginator(data, page, perPage)
  }

  public sortData(sort, data){
    if(sort){
      switch (sort) {
        case 'Newest':
          data = data.sort((a, b)=> {return <any>new Date(b.published) - <any>new Date(a.published)});           
          break;
        case 'Oldest':
          data = data.sort((a, b)=> {return <any>new Date(a.published) - <any>new Date(b.published)});           
          break;
        case 'Popular':
          data = data.sort((a, b) => { 
            if(a.ratingsValue/a.ratingsCount < b.ratingsValue/b.ratingsCount){
              return 1;
            }
            if(a.ratingsValue/a.ratingsCount > b.ratingsValue/b.ratingsCount){
              return -1;
            }
            return 0; 
          });
          break;
        case 'Price (Low to High)':
          if(this.appSettings.settings.currency == 'USD'){
            data = data.sort((a,b) => {
              if((a.priceDollar.sale || a.priceDollar.rent) > (b.priceDollar.sale || b.priceDollar.rent)){
                return 1;
              }
              if((a.priceDollar.sale || a.priceDollar.rent) < (b.priceDollar.sale || b.priceDollar.rent)){
                return -1;
              }
              return 0;  
            }) 
          }
          if(this.appSettings.settings.currency == 'EUR'){
            data = data.sort((a,b) => {
              if((a.priceEuro.sale || a.priceEuro.rent) > (b.priceEuro.sale || b.v.rent)){
                return 1;
              }
              if((a.priceEuro.sale || a.priceEuro.rent) < (b.priceEuro.sale || b.priceEuro.rent)){
                return -1;
              }
              return 0;  
            }) 
          }
          break;
        case 'Price (High to Low)':
          if(this.appSettings.settings.currency == 'USD'){
            data = data.sort((a,b) => {
              if((a.priceDollar.sale || a.priceDollar.rent) < (b.priceDollar.sale || b.priceDollar.rent)){
                return 1;
              }
              if((a.priceDollar.sale || a.priceDollar.rent) > (b.priceDollar.sale || b.priceDollar.rent)){
                return -1;
              }
              return 0;  
            }) 
          }
          if(this.appSettings.settings.currency == 'EUR'){
            data = data.sort((a,b) => {
              if((a.priceEuro.sale || a.priceEuro.rent) < (b.priceEuro.sale || b.v.rent)){
                return 1;
              }
              if((a.priceEuro.sale || a.priceEuro.rent) > (b.priceEuro.sale || b.priceEuro.rent)){
                return -1;
              }
              return 0;  
            }) 
          }
          break;
        default:
          break;
      }
    }
    return data;
  }

  public paginator(items, page?, perPage?) { 
    var page = page || 1,
    perPage = perPage || 4,
    offset = (page - 1) * perPage,   
    paginatedItems = items.slice(offset).slice(0, perPage),
    totalPages = Math.ceil(items.length / perPage);
    return {
      data: paginatedItems,
      pagination:{
        page: page,
        perPage: perPage,
        prePage: page - 1 ? page - 1 : null,
        nextPage: (totalPages > page) ? page + 1 : null,
        total: items.length,
        totalPages: totalPages,
      }
    };
  }



  public getTestimonials(){
    return [
        { 
            text: 'Donec molestie turpis ut mollis efficitur. Nam fringilla libero vel dictum vulputate. In malesuada, ligula non ornare consequat, augue nibh luctus nisl, et lobortis justo ipsum nec velit. Praesent lacinia quam ut nulla gravida, at viverra libero euismod. Sed tincidunt tempus augue vitae malesuada. Vestibulum eu lectus nisi. Aliquam erat volutpat.', 
            author: 'Mr. Adam Sandler', 
            position: 'General Director', 
            image: 'assets/images/profile/adam.jpg' 
        },
        { 
            text: 'Donec molestie turpis ut mollis efficitur. Nam fringilla libero vel dictum vulputate. In malesuada, ligula non ornare consequat, augue nibh luctus nisl, et lobortis justo ipsum nec velit. Praesent lacinia quam ut nulla gravida, at viverra libero euismod. Sed tincidunt tempus augue vitae malesuada. Vestibulum eu lectus nisi. Aliquam erat volutpat.', 
            author: 'Ashley Ahlberg', 
            position: 'Housewife', 
            image: 'assets/images/profile/ashley.jpg' 
        },
        { 
            text: 'Donec molestie turpis ut mollis efficitur. Nam fringilla libero vel dictum vulputate. In malesuada, ligula non ornare consequat, augue nibh luctus nisl, et lobortis justo ipsum nec velit. Praesent lacinia quam ut nulla gravida, at viverra libero euismod. Sed tincidunt tempus augue vitae malesuada. Vestibulum eu lectus nisi. Aliquam erat volutpat.', 
            author: 'Bruno Vespa', 
            position: 'Blogger', 
            image: 'assets/images/profile/bruno.jpg' 
        },
        { 
            text: 'Donec molestie turpis ut mollis efficitur. Nam fringilla libero vel dictum vulputate. In malesuada, ligula non ornare consequat, augue nibh luctus nisl, et lobortis justo ipsum nec velit. Praesent lacinia quam ut nulla gravida, at viverra libero euismod. Sed tincidunt tempus augue vitae malesuada. Vestibulum eu lectus nisi. Aliquam erat volutpat.', 
            author: 'Mrs. Julia Aniston', 
            position: 'Marketing Manager', 
            image: 'assets/images/profile/julia.jpg' 
        }
    ];
  }

  public getAgents(){
    return [        
        { 
            id: 1,
            fullName: 'Lusia Manuel',
            desc: 'Phasellus sed metus leo. Donec laoreet, lacus ut suscipit convallis, erat enim eleifend nulla, at sagittis enim urna et lacus.',            
            organization: 'POSHLIFE',
            email: 'lusia.m@housekey.com',
            phone: '(224) 267-1346',
            social: {
              facebook: 'lusia',
              twitter: 'lusia',
              linkedin: 'lusia',
              instagram: 'lusia',
              website: 'https://lusia.manuel.com'
            },
            ratingsCount: 6,
            ratingsValue: 480,
            image: 'assets/images/agents/a-1.jpg' 
        },
        { 
            id: 2,
            fullName: 'Andy Warhol',
            desc: 'Phasellus sed metus leo. Donec laoreet, lacus ut suscipit convallis, erat enim eleifend nulla, at sagittis enim urna et lacus.',            
            organization: 'HouseKey',
            email: 'andy.w@housekey.com',
            phone: '(212) 457-2308',
            social: {
              facebook: '',
              twitter: '',
              linkedin: '',
              instagram: '',
              website: 'https://andy.warhol.com'
            },
            ratingsCount: 4,
            ratingsValue: 400,
            image: 'assets/images/agents/a-2.jpg' 
        },        
        { 
            id: 3,
            fullName: 'Tereza Stiles',
            desc: 'Phasellus sed metus leo. Donec laoreet, lacus ut suscipit convallis, erat enim eleifend nulla, at sagittis enim urna et lacus.',            
            organization: 'HouseKey',
            email: 'tereza.s@housekey.com',
            phone: '(214) 617-2614',
            social: {
              facebook: '',
              twitter: '',
              linkedin: '',
              instagram: '',
              website: 'https://tereza.stiles.com'
            },
            ratingsCount: 4,
            ratingsValue: 380,
            image: 'assets/images/agents/a-3.jpg' 
        },
        { 
          id: 4,
          fullName: 'Michael Blair',
          desc: 'Phasellus sed metus leo. Donec laoreet, lacus ut suscipit convallis, erat enim eleifend nulla, at sagittis enim urna et lacus.',            
          organization: 'HouseKey',
          email: 'michael.b@housekey.com',
          phone: '(267) 388-1637',
          social: {
            facebook: '',
            twitter: '',
            linkedin: '',
            instagram: '',
            website: 'https://michael.blair.com'
          },
          ratingsCount: 6,
          ratingsValue: 480,
          image: 'assets/images/agents/a-4.jpg'  
        },
        { 
            id: 5,
            fullName: 'Michelle Ormond',
            desc: 'Phasellus sed metus leo. Donec laoreet, lacus ut suscipit convallis, erat enim eleifend nulla, at sagittis enim urna et lacus.',            
            organization: 'HouseKey',
            email: 'michelle.o@housekey.com',
            phone: '(267) 388-1637',
            social: {
              facebook: '',
              twitter: '',
              linkedin: '',
              instagram: '',
              website: 'https://michelle.ormond.com'
            },
            ratingsCount: 6,
            ratingsValue: 480, 
            image: 'assets/images/agents/a-5.jpg' 
        }
    ];
  }



  public getClients(){
    return [  
        { name: 'aloha', image: 'assets/images/clients/aloha.png' },
        { name: 'dream', image: 'assets/images/clients/dream.png' },  
        { name: 'congrats', image: 'assets/images/clients/congrats.png' },
        { name: 'best', image: 'assets/images/clients/best.png' },
        { name: 'original', image: 'assets/images/clients/original.png' },
        { name: 'retro', image: 'assets/images/clients/retro.png' },
        { name: 'king', image: 'assets/images/clients/king.png' },
        { name: 'love', image: 'assets/images/clients/love.png' },
        { name: 'the', image: 'assets/images/clients/the.png' },
        { name: 'easter', image: 'assets/images/clients/easter.png' },
        { name: 'with', image: 'assets/images/clients/with.png' },
        { name: 'special', image: 'assets/images/clients/special.png' },
        { name: 'bravo', image: 'assets/images/clients/bravo.png' }
    ];
  }


}
