import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Property } from 'src/app/app.models';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';

import { VisitFormModule } from 'src/app/shared/visit-form/visit-form.component';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-visit-details',
  templateUrl: './visit-details.component.html',
  styleUrls: ['./visit-details.component.scss']
})
export class VisitDetailsComponent implements OnInit {
  displayedColumns: string[] = ['Followup_date', 'user_comment', 'broker_comment', 'Status'];
  dataSource: MatTableDataSource<Property>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public sub: any;
  public selected = 'option1';
  public userSub:any;
  public id:number;
  constructor(private activatedRoute: ActivatedRoute,public appService:AppService,public dialog: MatDialog, public router:Router,public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      if(this.id){
        this.getAllVisitDetails();
      }
    });
  }

  getAllVisitDetails(){
    this.appService.getSchedule(this.id.toString()).subscribe(res => {
      if(res.code == 200 && res.status == "success"){
        this.userSub = res.data;
        this.dataSource = new MatTableDataSource(res.data.scheduleVisitRecords);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    },err => {
      if(err.status == 401){
        this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
        setTimeout(() => {
            this.router.navigate(['/login']);
        }, 5000);
      }
    });
  }
  
  public remove(property:Property) {
    const index: number = this.dataSource.data.indexOf(property);    
    if (index !== -1) {
      this.dataSource.data.splice(index,1);
      this.dataSource = new MatTableDataSource<Property>(this.dataSource.data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    } 
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(VisitFormModule, {
      width: '200vh',
      data: {id: this.userSub.id, email: this.userSub.email, propertyId: this.userSub.propertyId}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed-----',result);
      if(this.id){
        this.getAllVisitDetails();
      }
    });
  }
}
