import { Component, OnInit, ViewChild, HostListener,ChangeDetectorRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { LoginService } from 'src/app/login.service';
import { UserData } from 'src/app/app.models';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  public psConfig: PerfectScrollbarConfigInterface = {
    wheelPropagation:true
  };
  @ViewChild('sidenav') sidenav: any;
  public sidenavOpen:boolean = true;
  public user:UserData;
  public userRole= "user";
  public profileImage = "";
  public linksUser = [ 
    { name: 'Profile', href: 'profile', icon: 'person' },
    { name: 'Favorites', href: 'favorites', icon: 'favorite' },
    { name: 'Subscribe', href: 'subscribe-view', icon: 'add_circle' }, 
    // { name: 'Submit Property', href: '/submit-property', icon: 'add_circle' }  
  ]; 
  public linksAdmin = [  
    { name: 'User management', href: 'user-list', icon: 'person' },
    { name: 'Scheduled Visits', href: 'schduled-visits', icon: 'view_list' },
    { name: 'Blog Update', href: 'blog-update', icon: 'view_list' },
    { name: 'Mortgage Settings', href: 'mortgage-settings', icon: 'view_list' } 
  ]; 
  //Mortgage Settings
  constructor(public appService:AppService, public router:Router, public loginService: LoginService,private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    if(window.innerWidth < 960){
      this.sidenavOpen = false;
    };
    this.user = this.loginService.getUser();
    if(this.user && this.user.userVM.role){
      this.userRole = this.user.userVM.role;
    }else{
      this.router.navigate(['/login']);
    }
  }

  ngAfterViewChecked(){
    //your code to update the model
    this.cdr.detectChanges();
  }

  @HostListener('window:resize')
  public onWindowResize():void {
    (window.innerWidth < 960) ? this.sidenavOpen = false : this.sidenavOpen = true;
  }

  ngAfterViewInit(){
    this.appService.currentImage.subscribe(profileImage => this.profileImage = profileImage);
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {  
        if(window.innerWidth < 960){
          this.sidenav.close(); 
        }
      }                
    });
  } 
}
