import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Property, Pagination } from 'src/app/app.models';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Settings } from 'src/app/app.settings';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {
  displayedColumns: string[] = ['id', 'image', 'title', 'actions' ];
  dataSource: MatTableDataSource<Property>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public viewType: string = 'grid';
  public propertiesnew: Property[];
  public propertiesStatus: any;
  public pageNumber: number = 1;
  public message:string;
  public viewCol: number = 33.33;
  public settings: Settings;
  public pagination:Pagination = new Pagination(1, 8, null, 2, 0, 0); 
  constructor(public appService:AppService) { }

  ngOnInit() {
    this.propertiesnew = [];
    this.dataSource = new MatTableDataSource(this.appService.Data.favorites);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.appService.getFavorites().subscribe(data => {
      this.showData(data);
    })
  }

  public remove(property:Property) {
    const index: number = this.dataSource.data.indexOf(property);    
    if (index !== -1) {
      this.dataSource.data.splice(index,1);
      this.dataSource = new MatTableDataSource<Property>(this.dataSource.data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    } 
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
        this.appService.changeState("profile");
    }, 500);
  }
  refreshList(){
    this.appService.getFavorites().subscribe(data => {
      this.showData(data);
    })
  }
  showData(data:any){
    this.propertiesStatus = data;
    if(this.propertiesStatus.status == 'success'){
      if(this.propertiesStatus.data){
        for (let i = 0; i < this.propertiesStatus.data.length; i++) {
          if(!this.propertiesStatus.data[i].propertyImage){
            this.propertiesStatus.data[i].gallery = [         
                {id:1,
                    "small": "assets/images/props/flat-1/1-small.jpg",
                    "medium": "assets/images/props/flat-1/1-medium.jpg",
                    "big": "assets/images/props/flat-1/1-big.jpg"
                }
            ];
          }else{
            this.propertiesStatus.data[i].gallery = [];
            this.propertiesStatus.data[i].propertyImage.forEach(element => {
              this.propertiesStatus.data[i].gallery.push({id:1,
                "small": element,
                "medium": element,
                "big": "assets/images/props/flat-1/1-big.jpg"
              });
            });
          }
        }  
      }
      if(this.pageNumber == 1){
        this.propertiesnew = this.propertiesStatus.data
      }else{
        this.propertiesnew = this.propertiesnew.concat(this.propertiesStatus.data);
      }  
    }  
  }
}
