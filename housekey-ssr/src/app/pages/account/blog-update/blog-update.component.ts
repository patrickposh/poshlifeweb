import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { emailValidator, matchingPasswords } from 'src/app/theme/utils/app-validators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserData } from 'src/app/app.models';
import { LoginService } from 'src/app/login.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-blog-update',
  templateUrl: './blog-update.component.html',
  styleUrls: ['./blog-update.component.scss']
})
export class BlogUpdateComponent implements OnInit {
  public infoForm: FormGroup;
  public user: UserData;
  public showLoader: boolean = false;
  constructor(public formBuilder: FormBuilder, public snackBar: MatSnackBar, public loginService: LoginService, public router: Router,public translate: TranslateService, public appService:AppService) { }

  ngOnInit() {
    this.user = this.loginService.getUser();
    this.infoForm = this.formBuilder.group({
      title: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      content: ['', Validators.compose([Validators.required, Validators.minLength(10)])],
      blogImage: null
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
        this.appService.changeState("profile");
    }, 500);
  }

  resetForm(){
    this.infoForm.reset();
    Object.keys(this.infoForm.controls).forEach(key => {
      this.infoForm.get(key).setErrors(null) ;
    });
  }

  public onInfoFormSubmit(values: Object): void {
    if (this.infoForm.valid) {
      if (values["blogImage"] != null) {
        var temp = localStorage.getItem('user');
        this.user = this.loginService.getUser();
        var token = 'Bearer ' + this.user.token;
        this.showLoader = true;
        this.appService.uploadBlogPicture(this.infoForm.get("blogImage").value[0].file, token).subscribe(data => {
          if (data != null) {
            let userData = {
              title: this.infoForm.get("title").value,
              content: this.infoForm.get("content").value,
              createdBy: "ADMIN",
              blogImage: data.fileName,
              createdDate: null
            };
            this.appService.addBlog(userData, token).subscribe(data => {
              this.showLoader = false;
              if(data.code == 200){
                this.resetForm();
                this.snackBar.open('Your blog information updated successfully!', '×', { panelClass: 'success', verticalPosition: 'top', duration: 3000 });
              }else{
                this.snackBar.open('Something went wrong!', '×', { panelClass: 'success', verticalPosition: 'top', duration: 3000 });
              }
            },err => {
              this.showLoader = false;
              if(err.status == 401){
                this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
                setTimeout(() => {
                    this.router.navigate(['/login']);
                }, 5000);
              }
            }); 
          }else{
            this.showLoader = false;
            this.snackBar.open('Something went wrong!', '×', { panelClass: 'success', verticalPosition: 'top', duration: 3000 });
          }
        },err => {
          this.showLoader = false;
          if(err.status == 401){
            this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
            setTimeout(() => {
                this.router.navigate(['/login']);
            }, 5000);
          }
        }); 
      } else {
        var token = 'Bearer ' + this.user.token;
        let userData = {
          title: this.infoForm.get("title").value,
          content: this.infoForm.get("content").value,
          createdBy: "ADMIN",
          blogImage: "",
          createdDate: null
        };
        this.showLoader = true;
        this.appService.addBlog(userData, token).subscribe(data => {
          if(data.code == 200){
            this.resetForm();
            this.snackBar.open('Your blog information updated successfully!', '×', { panelClass: 'success', verticalPosition: 'top', duration: 3000 });
          }else{
            this.snackBar.open('Something went wrong!', '×', { panelClass: 'success', verticalPosition: 'top', duration: 3000 });
          }
          this.showLoader = false;
        },err => {
          this.showLoader = false;
          if(err.status == 401){
            this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
            setTimeout(() => {
                this.router.navigate(['/login']);
            }, 5000);
          }
        });
      }
      console.log(values);
    }
  }
}
