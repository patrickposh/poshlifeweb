import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { InputFileModule } from 'ngx-input-file';
import { AgmCoreModule } from '@agm/core';  
import { AccountComponent } from './account.component';
import { DashboardComponent } from './dashboard/dashboard.component'; 
import { MyPropertiesComponent } from './my-properties/my-properties.component';
import { ScheduledVisitsComponent } from './schduled-visits/schduled-visits.component';
import { UserListComponent } from './user-list/user-list.component';
import { SubscribeComponent } from './subscribe-view/subscribe-view.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { ProfileComponent } from './profile/profile.component';
import { EditPropertyComponent } from './edit-property/edit-property.component';
import { Ng5SliderModule } from 'ng5-slider';
import { VisitDetailsComponent } from './visit-details/visit-details.component';
import { BlogUpdateComponent } from './blog-update/blog-update.component';
import { MortgageSettingsComponent } from './mortgage-settings/mortgage-settings.component';
import { TranslateModule } from '@ngx-translate/core';

export const routes = [
  { 
    path: '', 
    component: AccountComponent, children: [
      { path: '', redirectTo: 'profile', pathMatch: 'full' }, 
      { path: 'my-properties', component: MyPropertiesComponent },   
      { path: 'visit-details', component: VisitDetailsComponent },
      { path: 'visit-details/:id', component: VisitDetailsComponent },
      { path: 'schduled-visits', component: ScheduledVisitsComponent },
      { path: 'user-list', component: UserListComponent },
      { path: 'subscribe-view', component: SubscribeComponent },
      { path: 'my-properties/:id', component: EditPropertyComponent },
      { path: 'favorites', component: FavoritesComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'blog-update', component: BlogUpdateComponent },
      { path: 'mortgage-settings', component: MortgageSettingsComponent }
    ]
  }
];

@NgModule({
  declarations: [
    DashboardComponent,
    AccountComponent,  
    MyPropertiesComponent,
    ScheduledVisitsComponent,
    UserListComponent,
    SubscribeComponent,
    FavoritesComponent, 
    ProfileComponent, 
    EditPropertyComponent,
    VisitDetailsComponent,
    BlogUpdateComponent,
    MortgageSettingsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    Ng5SliderModule,
    TranslateModule,
    InputFileModule,
    AgmCoreModule
  ]
})
export class AccountModule { }
