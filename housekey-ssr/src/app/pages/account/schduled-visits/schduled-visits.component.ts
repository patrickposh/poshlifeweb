import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Property } from 'src/app/app.models';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-schduled-visits',
  templateUrl: './schduled-visits.component.html',
  styleUrls: ['./schduled-visits.component.scss']
})
export class ScheduledVisitsComponent implements OnInit {
  displayedColumns: string[] = ['Name', 'Contact', 'PropertyID', 'Status', 'Comments', 'Followup_date', 'actions' ];
  dataSource: MatTableDataSource<Property>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  constructor(public appService:AppService, public router:Router,public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.appService.getAllSchduledVisits().subscribe(res => {
      if(res.code == 200 && res.status == "success"){
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    },err => {
      if(err.status == 401){
        this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
        setTimeout(() => {
            this.router.navigate(['/login']);
        }, 5000);
      }
		}); 
  }
  
  public remove(property:Property) {
    const index: number = this.dataSource.data.indexOf(property);    
    if (index !== -1) {
      this.dataSource.data.splice(index,1);
      this.dataSource = new MatTableDataSource<Property>(this.dataSource.data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    } 
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}