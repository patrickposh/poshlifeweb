import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { emailValidator } from 'src/app/theme/utils/app-validators';
import { LoginService } from 'src/app/login.service';
import { LabelType, Options } from 'ng5-slider';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
declare const google: any;

@Component({
    selector: 'app-subscribe-view',
    templateUrl: './subscribe-view.component.html',
    styleUrls: ['./subscribe-view.component.scss']
})

export class SubscribeComponent implements OnInit {
    public prTypeStatus = "";
    public bathrooms = [];
    public parkings = [];
    public bedrooms = new Set();
    public isForSale: boolean = true;
    public propertyOptionsComm = [];
    public propertyOptionsRes = [];
    public basementStyles = [];
    public contactForm: FormGroup;
    public options: Options = {
        floor: 500,
        ceil: 20000
    };
    public optionsArea: Options = {
        floor: 1,
        ceil: 10
    };
    public optionsNew: Options = {
        floor: 500,
        ceil: 1000000,
        translate: (value: number, label: LabelType): string => {
            return '$' + value.toLocaleString('en');
        }
    };
    value: number = 0;
    highValue: number = 0;
    valueNew: number = 0;
    highValueNew: number = 0;
    valueArea: number = 1;
    highValueArea: number = 1;
    public lat: number = 40.678178;
    public lng: number = -73.944158;
    pointList: { lat: number; lng: number }[] = [];
    drawingManager: any;
    selectedShape: any;
    selectedArea = 0;
    name: any;
    email: any;
    phone: any;
    polyData: any = [];
    userId = null;
    public showLoader: boolean = false;

    constructor(public fb: FormBuilder, public appService: AppService,
        public translate: TranslateService, public snackBar: MatSnackBar,
        public loginService: LoginService,public router: Router) {}

    ngOnInit() {
        var user = this.loginService.getUser();
        if (user) {
            this.userId = user.userVM.id;
            this.name = user.userVM.firstname + ' ' + user.userVM.lastname;
            this.email = user.userVM.email;
            this.phone = user.userVM.phoneNumber;
        }
        this.propertyOptionsComm = this.appService.getPropertyTypesComm();
        this.propertyOptionsRes = this.appService.getPropertyTypesRes();
        this.basementStyles = this.appService.getBasementType();
        this.prTypeStatus = "residential";
        this.contactForm = this.fb.group({
            sliderControlGroup: this.fb.group({
                sliderControl: new FormControl([500, 500]),
            }),
            sliderControlGroupNew: this.fb.group({
                sliderControlNew: new FormControl([0, 0]),
            }),
            propertyType: null,
            propertyOption: null,
            saleLease: 'Sale',
            bedrooms: [],
            washrooms: null,
            basementStyle: null,
            parkingSpaces: null,
            message: null,
            address: new FormControl()
        });

        this.setCurrentPosition();
    }

    addBedrooms(type: string) {
        if (this.bedrooms.has(type)) {
            this.bedrooms.delete(type);
        } else if (type === 'All') {
            this.bedrooms.clear();
        } else {
            this.bedrooms.add(type);
        }
        this.contactForm.patchValue({
            bedrooms: this.bedrooms
        })
    }

    addBathrooms(type: string) {
        if (type == "All") {
            this.bathrooms = [];
            this.contactForm.patchValue({
                washrooms: "0"
            })
        } else {
            this.bathrooms = [];
            this.bathrooms.push(type);
            this.contactForm.patchValue({
                washrooms: this.bathrooms[0]
            })
        }
    }

    addParkings(type: string) {
        this.parkings = [];
        this.parkings.push(type);
        this.contactForm.patchValue({
            parkingSpaces: this.parkings[0]
        })
    }

    changePrType(type: string) {
        if (type == 'sale') {
            this.contactForm.patchValue({
                saleLease: "Sale"
            })
            this.isForSale = true;
        } else if (type == 'rent') {
            this.contactForm.patchValue({
                saleLease: "Lease"
            })
            this.isForSale = false;
        }
    }

    private setCurrentPosition() {
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.lat = position.coords.latitude;
                this.lng = position.coords.longitude;
            });
        }
    }

    onMapReady(map) {
        this.initDrawingManager(map);
    }

    initDrawingManager = (map: any) => {
        const self = this;
        const options = {
            drawingControl: true,
            drawingControlOptions: {
                drawingModes: ['polygon'],
            },
            polygonOptions: {
                draggable: true,
                editable: true,
            },
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
        };
        this.drawingManager = new google.maps.drawing.DrawingManager(options);
        this.drawingManager.setMap(map);
        google.maps.event.addListener(
            this.drawingManager,
            'overlaycomplete',
            (event) => {
                if (event.type === google.maps.drawing.OverlayType.POLYGON) {
                    const paths = event.overlay.getPaths();
                    for (let p = 0; p < paths.getLength(); p++) {
                        google.maps.event.addListener(
                            paths.getAt(p),
                            'set_at',
                            () => {
                                if (!event.overlay.drag) {
                                    self.updatePointList(event.overlay.getPath());
                                }
                            }
                        );
                        google.maps.event.addListener(
                            paths.getAt(p),
                            'insert_at',
                            () => {
                                self.updatePointList(event.overlay.getPath());
                            }
                        );
                        google.maps.event.addListener(
                            paths.getAt(p),
                            'remove_at',
                            () => {
                                self.updatePointList(event.overlay.getPath());
                            }
                        );
                    }
                    self.updatePointList(event.overlay.getPath());
                    this.selectedShape = event.overlay;
                    this.selectedShape.type = event.type;
                }
                if (event.type !== google.maps.drawing.OverlayType.MARKER) {
                    // Switch back to non-drawing mode after drawing a shape.
                    self.drawingManager.setDrawingMode(null);
                    // To hide:
                    self.drawingManager.setOptions({
                        drawingControl: false,
                    });
                }
            }
        );
    }

    deleteSelectedShape() {
        if (this.selectedShape) {
            this.selectedShape.setMap(null);
            this.selectedArea = 0;
            this.pointList = [];
            // To show:
            this.drawingManager.setOptions({
                drawingControl: true,
            });
        }
    }

    updatePointList(path) {
        this.pointList = [];
        const len = path.getLength();
        for (let i = 0; i < len; i++) {
            this.pointList.push(
                path.getAt(i).toJSON()
            );
        }
        this.selectedArea = google.maps.geometry.spherical.computeArea(
            path
        );
        let tempArr = [];
        this.pointList.forEach(element => {
            var data = {
                latitude: element.lat,
				longitude: element.lng
            }
            tempArr.push(data);
        });
        this.polyData = tempArr;
    }

    onContactFormSubmit(value) {
        var beds :any;
        if(value.bedrooms){
            beds = Array.from(value.bedrooms); 
        }
        var temp = {
            "name": this.name,
            "email": this.email,
            "phone": this.phone,
            "userId": this.userId,
            "message": value.message,
            "propertyOption": value.propertyOption,
            "propertyType": value.propertyType,
            "saleLease": value.saleLease,
            "washrooms": value.washrooms,
            "basementStyle": value.basementStyle,
            "bedrooms": beds,
            "parkingSpaces": value.parkingSpaces,
            "areaMin": value.sliderControlGroup.sliderControl[0] == 500 ? '' : value.sliderControlGroup.sliderControl[0],
            "areaMax": value.sliderControlGroup.sliderControl[1] == 500 ? '' : value.sliderControlGroup.sliderControl[1],
            "priceMin": value.sliderControlGroupNew.sliderControlNew[0] == 500 ? '' : value.sliderControlGroupNew.sliderControlNew[0],
            "priceMax": value.sliderControlGroupNew.sliderControlNew[1] == 500 ? '' : value.sliderControlGroupNew.sliderControlNew[1],
            "isActive":true,
            "polygonData": this.polyData
        }
        this.showLoader = true;
        this.appService.subscribeToNewsLatter(temp).subscribe(data =>{
            this.contactForm.get('sliderControlGroup').patchValue({'sliderControl':[0,0]});
            this.contactForm.get('sliderControlGroupNew').patchValue({'sliderControlNew':[0,0]});
            this.parkings = [];
            this.bathrooms = [];
            this.prTypeStatus = "residential";
            this.bedrooms.clear();
            this.contactForm.reset();
            Object.keys(this.contactForm.controls).forEach(key => {
                this.contactForm.get(key).setErrors(null) ;
            });
            this.showLoader = false;
            this.snackBar.open('Successfully subscribed to daily updates !', '×', { panelClass: 'success', verticalPosition: 'top', duration: 3000 });
            setTimeout(() => {
                this.router.navigate(['/']);
            }, 2000);
        },err =>{
            this.showLoader = false;
        });
    }
}