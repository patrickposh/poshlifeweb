import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { emailValidator, matchingPasswords } from 'src/app/theme/utils/app-validators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserData } from 'src/app/app.models';
import { LoginService } from 'src/app/login.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public infoForm: FormGroup;
  public passwordForm: FormGroup;
  public user: UserData;
  constructor(public formBuilder: FormBuilder, public snackBar: MatSnackBar, public loginService: LoginService, public router: Router,public translate: TranslateService, public appService:AppService) { }

  ngOnInit() {
    this.user = this.loginService.getUser();
    this.infoForm = this.formBuilder.group({
      firstname: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      lastname: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      email: ['', Validators.compose([Validators.required, emailValidator])],
      phoneNumber: ['', Validators.required],
      imageName: null
    });
    this.passwordForm = this.formBuilder.group({
      newPassword: ['', Validators.required],
      confirmNewPassword: ['', Validators.required]
    }, { validator: matchingPasswords('newPassword', 'confirmNewPassword') });

    if (this.user) {
      this.infoForm.patchValue({
        firstname: this.user.userVM.firstname,
        lastname: this.user.userVM.lastname,
        email: this.user.userVM.email,
        phoneNumber: this.user.userVM.phoneNumber,
      })
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
        this.appService.changeState("profile");
    }, 500);
  }

  public onInfoFormSubmit(values: Object): void {
    this.user = this.loginService.getUser();
    if (this.infoForm.valid) {
      if (values["imageName"] != null && this.infoForm.get("imageName").value[0]) {
        var token = 'Bearer ' + this.user.token;
        this.loginService.uploadUserPicture(this.infoForm.get("imageName").value[0].file, token).subscribe(data => {
          if (data != null) {
            let userData = {
              firstname: this.infoForm.get("firstname").value,
              lastname: this.infoForm.get("lastname").value,
              email: this.infoForm.get("email").value,
              phoneNumber: this.infoForm.get("phoneNumber").value,
              id: this.user.userVM.id,
              imageName: data.fileName
            };
            this.loginService.updateUser(userData, token).subscribe(data => {
              if(data.status == "success" && data.data){
                this.updateUserInfo(data.data);
              }
            },err => {
              if(err.status == 401){
                this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
                setTimeout(() => {
                    this.router.navigate(['/login']);
                }, 5000);
              }
            }); 
          }
        },err => {
          if(err.status == 401){
            this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
            setTimeout(() => {
                this.router.navigate(['/login']);
            }, 5000);
          }
        }); 
      } else {
        var token = 'Bearer ' + this.user.token;
        var id = this.user.userVM.id;
        let userData = {
          firstname: this.infoForm.get("firstname").value,
          lastname: this.infoForm.get("lastname").value,
          email: this.infoForm.get("email").value,
          phoneNumber: this.infoForm.get("phoneNumber").value,
          id: this.user.userVM.id
        };
        this.loginService.updateUser(userData, token).subscribe(data => {
          if(data.status == "success" && data.data){
            this.updateUserInfo(data.data);
          }
        },err => {
          if(err.status == 401){
            this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
            setTimeout(() => {
                this.router.navigate(['/login']);
            }, 5000);
          }
        }); 
      }
      this.snackBar.open('Your account information updated successfully!', '×', { panelClass: 'success', verticalPosition: 'top', duration: 3000 });
    }
  }

  updateUserInfo(data:any){
      var tempUser = this.loginService.getUser();
      if(data.imageName){
        tempUser.userVM.imageName = data.imageName;
        if(!tempUser.userVM){
          this.appService.changeProfileImage('');
        }else{
          this.appService.changeProfileImage(tempUser.userVM.imageName);
        }
      }
      tempUser.userVM.email = data.email;
      tempUser.userVM.firstname = data.firstname;
      tempUser.userVM.lastname = data.lastname;
      tempUser.userVM.phoneNumber = data.phoneNumber;
      tempUser.userVM.modifiedDate = data.modifiedDate;
      this.loginService.setUser(tempUser);
  }

  public onPasswordFormSubmit(values: Object): void {
    if (this.passwordForm.valid) {
      var details = {
        email: this.infoForm.get("email").value,
        password: values["newPassword"]
      };
      var token = 'Bearer ' + this.user.token;
      this.loginService.reset(details, token).subscribe(data => {
        if (data.code == 200) {
          this.snackBar.open('Your password changed successfully!', '×', { panelClass: 'success', verticalPosition: 'top', duration: 3000 });
          this.router.navigate(['/login']);
        }
      },err => {
        if(err.status == 401){
          this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
          setTimeout(() => {
              this.router.navigate(['/login']);
          }, 5000);
        }
      }); 
      
    }
  }
}
