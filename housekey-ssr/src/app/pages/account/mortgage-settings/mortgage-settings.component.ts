import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { emailValidator, matchingPasswords } from 'src/app/theme/utils/app-validators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserData } from 'src/app/app.models';
import { LoginService } from 'src/app/login.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-mortgage-settings',
  templateUrl: './mortgage-settings.component.html',
  styleUrls: ['./mortgage-settings.component.scss']
})
export class MortgageSettingsComponent implements OnInit {
  public infoForm: FormGroup;
  public user: UserData;
  public showLoader: boolean = false;
  constructor(public formBuilder: FormBuilder, public snackBar: MatSnackBar, public loginService: LoginService, public router: Router,public translate: TranslateService, public appService:AppService) { }

  ngOnInit() {
    this.user = this.loginService.getUser();
    this.infoForm = this.formBuilder.group({
      interestRate: ['', Validators.compose([Validators.required, Validators.pattern("^[0-9].[0-9]*$")])]
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
        this.appService.changeState("profile");
    }, 500);
    this.showLoader = true;
    this.appService.getRateOfInterest('RateOfInterest').subscribe(data => {
      this.showLoader = false;
      if(data.code == 200 && data.status == "success"){
        this.infoForm.patchValue({
          interestRate : data.data.taxValue
        })
      }
    },err => {
      this.showLoader = false;
      if(err.status == 401){
        this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
        setTimeout(() => {
            this.router.navigate(['/login']);
        }, 5000);
      }
    });
  }

  public onInfoFormSubmit(values: Object): void {
    if (this.infoForm.valid) {
      var token = 'Bearer ' + this.user.token;
        let userData = {
          taxName: "RateOfInterest",
          taxValue: this.infoForm.get("interestRate").value
        };
        this.showLoader = true;
        this.appService.updateRateOfInterest(userData, token).subscribe(data => {
          this.showLoader = false;
          if(data.code == 200 && data.status == "success"){
            this.snackBar.open('Your information updated successfully!', '×', { panelClass: 'success', verticalPosition: 'top', duration: 3000 });
          }
        },err => {
          this.showLoader = false;
          if(err.status == 401){
            this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
            setTimeout(() => {
                this.router.navigate(['/login']);
            }, 5000);
          }
        });
    }
  }
}
