import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AppService } from '../../app.service';
import { map } from "rxjs/operators";

@Injectable()
export class BlogService {

    options: any;
    token: any;

    constructor(public http: Http, public appService: AppService) {

        var loginDetails = JSON.parse(localStorage.getItem('user'));
        if (loginDetails != undefined) {
            this.token = loginDetails.token
        }

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Authorization', 'Bearer ' + this.token);
        this.options = new RequestOptions({ headers: headers });
    }

    getAllBlogs() {
        return this.http.get(this.appService.urlNew + 'api/blog/',
            this.options).pipe(map(response => response.json()));
    }

    getBlogDetail(blogId) {
        return this.http.get(this.appService.urlNew + 'api/blog/' + blogId,
            this.options).pipe(map(response => response.json()));
    }
}