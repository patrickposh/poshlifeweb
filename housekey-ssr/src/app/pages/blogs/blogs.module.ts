import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { SharedModule } from '../../shared/shared.module';
import { BlogsComponent } from './blogs.component'; 
import { BlogComponent } from './blog/blog.component';
import { BlogService } from './blogs.service';
import { TranslateModule } from '@ngx-translate/core';

export const routes = [
  { path: '', component: BlogsComponent, pathMatch: 'full' }, 
  { path: ':blogId', component: BlogComponent }
];

@NgModule({
  declarations: [
    BlogsComponent,
    BlogComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
		HttpModule,
    TranslateModule,
    RouterModule.forChild(routes),
    SharedModule,
  ],
  providers: [
    BlogService
  ]
})
export class BlogsModule { }
