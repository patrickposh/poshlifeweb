import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { BlogService } from '../blogs.service';

@Component({
	selector: 'app-blog',
	templateUrl: './blog.component.html',
	styleUrls: ['./blog.component.scss']
})

export class BlogComponent implements OnInit {

	blogId: any = null;
	blog: any;

	constructor(public router: Router, private activatedRoute: ActivatedRoute,
		private blogService: BlogService,public appService:AppService) {

		this.activatedRoute.params.subscribe((params: Params) => {
			this.blogId = parseInt(params['blogId']);
		});
	}

	ngOnInit() {
		if(this.blogId != null && this.blogId != undefined) {
			this.getBlogDetails(this.blogId);
		}
	}

	getBlogDetails(blogId) {
		this.appService.getBlogDetail(blogId).subscribe(res =>{
			if (res.status == 200) {
				this.blog = res.data;
				this.blog.blogImagePath = this.appService.blogImageURl + this.blog.blogImage;
				console.log(this.blog);
			}
		},err => {
			console.log(err);
		});
	}

}
