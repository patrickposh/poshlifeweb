import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { LoginService } from 'src/app/login.service';
import { BlogService } from './blogs.service';

@Component({
	selector: 'app-blogs',
	templateUrl: './blogs.component.html',
	styleUrls: ['./blogs.component.scss']
})

export class BlogsComponent implements OnInit {

	blogs: any[] = [];
	public showLoader: boolean = false;

	constructor(public router: Router,private blogService: BlogService,public snackBar: MatSnackBar,public appService:AppService,public loginService: LoginService) { }

	ngOnInit() {
		if(this.loginService.getUser()){
			this.getAllBlogs();
		}else{
			this.snackBar.open('Login Required !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
			  setTimeout(() => {
				  this.router.navigate(['/login']);
			  }, 5000);
		}
	}

	getAllBlogs() {
		this.showLoader = true;
		this.appService.getAllBlogs().subscribe(async res => {
			this.showLoader = false;
			this.blogs = res.data;
			if(this.blogs){
				for (let i = 0; i < this.blogs.length; i++) {
					if(this.blogs[i].blogImage && this.blogs[i].blogImage != ''){
						this.blogs[i].blogImagePath = this.appService.blogImageURl + this.blogs[i].blogImage;
					}else{
						this.blogs[i].blogImagePath = "";
					}
				}
			}
		},err => {
			this.showLoader = false;
			if(err.status == 401){
			  this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
			  setTimeout(() => {
				  this.router.navigate(['/login']);
			  }, 5000);
			}
		});
	}

	viewBlog(blogId) {
		this.router.navigate(['blogs/' + blogId]);
	}
}
