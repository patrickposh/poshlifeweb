import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Property } from 'src/app/app.models';
import { AppService } from 'src/app/app.service';
import { emailValidator } from 'src/app/theme/utils/app-validators';

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss']
})
export class MapViewComponent implements OnInit {
  public lat: number = 40.678178;
  public lng: number = -73.944158;
  public zoomLevel: number = 12; 
  public properties: Property[];
  public minClusterSize: number = 2;
  public origin: {};
  public map: any;
  public destination: {};
  public currentLat:number;
  public currentLng:number;
  public renderOptions = {
    suppressMarkers: true,
  }

  public markerOptions = {
    origin: {
            draggable: true,
          },
    destination: {
              opacity: 0.8,
          },
  }
  public clusterStyles = [
    {
      textColor: 'black',
      url: 'assets/images/m3.png',
      height: 36,
      width: 36
    },
   {
      textColor: 'white',
      url: 'assets/images/m3.png',
      height: 36,
      width: 36
    },
   {
      textColor: 'white',
      url: 'assets/images/m3.png',
      height: 36,
      width: 36
    }
  ];

  constructor(public formBuilder: FormBuilder,public appService:AppService) { }

  ngOnInit() {
    var obj = this.appService.getSearchObject();
    this.appService.searchProperty(obj).subscribe(data => {
      
      this.properties = data.data;
     })
  }

  mapReady(map) {
    this.map = map;
    var self = this;
    this.map.addListener("dragend", function () {
      //do what you want
      self.currentLng = self.map.getCenter().lang
      self.currentLng = self.map.getCenter().lat();
    });
}

zoomChange(event){
  this.zoomLevel = event;
}

  centerChange(event)
    {
        let coords=JSON.stringify(event);
        let coords3=JSON.parse(coords);
    }

    markerProperties = [];
    clickOnCluster(cluster: any): void {
      var tempArr = [];
      if(this.zoomLevel == 18) {
        cluster.markers_.map(m => { 
          console.log(m); 
          tempArr.push(m.title)
        }); 
        console.log(tempArr);
        this.getMarkerProperties(tempArr);
      }
    }

    getMarkerProperties(data) {
      this.properties.forEach(element => {
        data.forEach(data => {
          if(data == element.id) {
            this.markerProperties.push(element);
          }
        });
      });
    }

}
