import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { AgmCoreModule } from '@agm/core'; 
import { MapViewComponent } from './map-view.component';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';

export const routes = [
  { path: '', component: MapViewComponent, pathMatch: 'full'  }
];

@NgModule({
  declarations: [MapViewComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    AgmCoreModule,
    SharedModule,
    AgmJsMarkerClustererModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCHCXkjFkYDOeqnO4Gwos55C73UmcaZdCw'
    })
  ]
})
export class MapViewModule { }
