import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
// import { Router } from '@angular/router';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { LoginService } from '../../../login.service';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  public activateForm: FormGroup;
  public hide = true;
  public errorFlag = false;
  public errorMessage: String = "";
  public successMessage: String = "";
  public urlstates:String[];
  constructor(public fb: FormBuilder, public router: Router, public loginService: LoginService, private route:ActivatedRoute,public snackBar: MatSnackBar) { }

  ngOnInit(): void {
    console.log(this.router);
    this.onactivateFormSubmit();
  }

  public onactivateFormSubmit(): void {
    this.errorFlag = false;
    
    this.urlstates=window.location.href.split("/");
    console.log(this.urlstates[this.urlstates.length-1]);
    var details = {
      email: this.urlstates[5]
    };
    var token='Bearer '+this.urlstates[7];
      this.loginService.activate(details,token).subscribe(data => {
        console.log("data", data);
        if (data.code === 200) {
          //this.router.navigate(['/']);
          this.successMessage = "Your account has been activated.";
          this.errorMessage = "";
        } else {
          this.successMessage = "";
          this.errorFlag = true;
          if (data.errorKey === "email400") {
            this.errorMessage = "This Email Id is Invalid."
          } else if (data.errorKey === "password400") {
            this.errorMessage = "This Password is Invalid."
          } else if (data.errorKey === "active400") {
            this.errorMessage = "Please Activate Your Account."
          }
        }
      },err => {
        if(err.status == 401){
          this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
          setTimeout(() => {
              this.router.navigate(['/login']);
          }, 5000);
        }
      });
  }

}
