import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user/user.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const routes = [
  { 
    
    path: '', 
    component: UserComponent, children: [
      { path: '', redirectTo: 'user', pathMatch: 'full' },
      { path: ':email/usertoken/:token', component: UserComponent } ]
  }
];

@NgModule({
  declarations: [UserComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class UserModule { }
