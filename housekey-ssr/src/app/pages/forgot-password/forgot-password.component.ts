import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../login.service';
import { emailValidator } from 'src/app/theme/utils/app-validators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  public loginForm: FormGroup;
  public showLoader: boolean = false;
  public errorMessage: String = "";
  public errorFlag = false;
  constructor(public fb: FormBuilder, public snackBar: MatSnackBar, public router: Router, public loginService: LoginService,private spinner: NgxSpinnerService,public translate: TranslateService) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, emailValidator])],
    });
    
  }

 
  public onLoginFormSubmit(values: Object): void {
    console.log(values);
    var details = {
      email: values['email']
    };
    this.showLoader = true;
    if (this.loginForm.valid) {
      this.loginService.forgotPassword(details).subscribe(data => {
        this.showLoader = false;
        if (data.code === 200 && data.data) {
          this.snackBar.open(data.data, '×', { panelClass: 'success', verticalPosition: 'top', duration: 6000 });
          this.errorMessage = data.data;
        } else {
          this.errorMessage = "Faild to process request";
        }
      });
    }
  }
}
