import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../login.service';
import { SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { TranslateService } from '@ngx-translate/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public hide = true;
  public errorFlag = false;
  public showLoader: boolean = false;
  public errorMessage: String = "";
  constructor(public fb: FormBuilder, public router: Router, public loginService: LoginService,private authService: SocialAuthService,public translate: TranslateService,public appService:AppService) { }

  ngOnInit() {
    this.loginService.removeUser();
    this.appService.changeMessage("no");
    this.loginForm = this.fb.group({
      username: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
      rememberMe: false
    });
    this.authService.initState.subscribe(() => {}, console.error, () => {console.log('--all providers are ready')});
  }

  signInWithGoogle(): void {
    this.authService.authState.subscribe((user) => {
      // this.user = user;
      // this.loggedIn = (user != null);
      this.showLoader = false;
      console.log("Google Email ...",user.email);
      console.log("Google name ...",user.name);
      console.log("Google firstName ...",user.firstName);
      console.log("Google lastName ...",user.lastName);
      console.log("Google photoUrl ...",user.photoUrl);
      console.log("Google id ...",user);
      this.socialLogin(user);
    });
    setTimeout(() => {
      this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    }, 500);
    
  }

  signInWithFB(): void {
    this.authService.authState.subscribe((user) => {
      // this.user = user;
      // this.loggedIn = (user != null);
      this.showLoader = false;
      console.log("Facebook Email ...",user.email);
      console.log("Facebook name ...",user.name);
      console.log("Facebook firstName ...",user.firstName);
      console.log("Facebook lastName ...",user.lastName);
      console.log("Facebook photoUrl ...",user.photoUrl);
      console.log("Facebook id ...",user);
      this.socialLogin(user);
    });
    setTimeout(() => {
      this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    }, 500);
  }
  socialLogin(user:any){
    var details = {
      email: user.email,
      imageName: user.photoUrl,
      firstname: user.firstName,
      lastname: user.lastName,
      active: true,
      socialId: user.id,
      socialType:  user.provider
    };
      this.showLoader = true;
      this.loginService.socialLogin(details).subscribe(data => {
        this.showLoader = false;
        if (data.code === 200 && data.data.token) {
          this.loginService.setUser(data.data);
          var tempUser = this.loginService.getUser();
          if(!tempUser.userVM){
            this.appService.changeProfileImage('');
          }else{
            this.appService.changeProfileImage(tempUser.userVM.imageName);
          }
          this.appService.changeUserRole(tempUser.userVM.role);
          this.router.navigate(['/']);
        } else {
          this.errorFlag = true;
        }
      });
  }
  
  public onLoginFormSubmit(values: Object): void {
    this.errorFlag = false;
    console.log(values);
    var details = {
      email: values['username'],
      password: values['password']
    };
    if (this.loginForm.valid) {
      this.showLoader = true;
      this.loginService.login(details).subscribe(data => {
        this.showLoader = false;
        if (data.code === 200 && data.data.token) {
          this.loginService.setUser(data.data);
          var tempUser = this.loginService.getUser();
          if(!tempUser.userVM){
            this.appService.changeProfileImage('');
          }else{
            this.appService.changeProfileImage(tempUser.userVM.imageName);
          }
          this.appService.changeUserRole(tempUser.userVM.role);
          this.router.navigate(['/']);
        } else {
          this.errorFlag = true;
          if (data.errorKey === "email400") {
            this.errorMessage = "This Email Id is Invalid."
          } else if (data.errorKey === "password400") {
            this.errorMessage = "This Password is Invalid."
          } else if (data.errorKey === "active400") {
            this.errorMessage = "Please Activate Your Account."
          }
          // this.router.navigate(['/not-found']);
        }
      });
    }
  }
}
