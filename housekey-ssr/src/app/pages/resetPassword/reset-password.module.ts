import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { ResetPassword } from './reset-password.component';

export const routes = [
  { path: '', component: ResetPassword, children: [
    { path: '', redirectTo: 'user', pathMatch: 'full' },
    { path: ':email/usertoken/:token', component: ResetPassword } ]}
];

@NgModule({
  declarations: [ResetPassword],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class ResetPasswordModule { }
