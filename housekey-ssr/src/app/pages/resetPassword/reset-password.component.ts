import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { Router } from '@angular/router';
import { emailValidator, matchingPasswords } from 'src/app/theme/utils/app-validators';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { LoginService } from 'src/app/login.service';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-user',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPassword implements OnInit {
  public passwordForm: FormGroup;
  public hide = true;
  public errorFlag = false;
  public errorMessage: String = "";
  public successMessage: String = "";
  public urlstates: String[];
  constructor(public formBuilder: FormBuilder, public router: Router, public loginService: LoginService, private route: ActivatedRoute,public snackBar: MatSnackBar) { }

  ngOnInit(): void {
    console.log(this.router);
    this.passwordForm = this.formBuilder.group({
      // currentPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmNewPassword: ['', Validators.required]
    }, { validator: matchingPasswords('newPassword', 'confirmNewPassword') });
  }

  public onPasswordFormSubmit(value) {
    this.urlstates = window.location.href.split("/");
    var details = {
      email: this.urlstates[5],
      password: this.passwordForm.get("newPassword").value
    };
    var token = 'Bearer ' + this.urlstates[7];
    this.loginService.reset(details, token).subscribe(data => {
      if(data.code == 200){
        this.router.navigate(['/login']);
      }
    },err => {
      if(err.status == 401){
        this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
        setTimeout(() => {
            this.router.navigate(['/login']);
        }, 5000);
      }
    }); 
  }

}
