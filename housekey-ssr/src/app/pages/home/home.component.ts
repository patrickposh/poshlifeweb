import { Component, OnInit, Input, HostListener } from '@angular/core';
import { Settings, AppSettings } from '../../app.settings';
import { AppService } from '../../app.service';
import { Property, Pagination, SearchObj, LocationC } from '../../app.models';
import { NgxSpinnerService } from "ngx-spinner";
import { Subscription } from 'rxjs';
import { MediaChange, MediaObserver } from '@angular/flex-layout'; 
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/login.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    // Do more processing...
    localStorage.removeItem("searchedData");
    localStorage.setItem("pageNumber", "1");
    localStorage.setItem("totalProperties", "");
  }


  @Input() isHomePage:boolean = false;
  watcher: Subscription;
  activeMediaQuery = '';
  public slides = [];
  public propertiesStatus: any;
  public propertiesCount: any;
  public propertiesnew: Property[];
  public viewType: string = 'grid';
  public viewCol: number = 25;
  public count: number = 8;
  public pageNumber: number = 1;
  public sort: string;
  public searchObj: SearchObj;
  public currentLoc: LocationC;
  public searchFields: any;
  public removedSearchField: string;
  public pagination:Pagination = new Pagination(1, 8, null, 2, 0, 0); 
  public message:string;
  public featuredProperties: Property[];
  public settings: Settings;
  public showLoader: boolean = false;
  public pageRefreshed: boolean = true;
  public previousUrl = '';
  subscription: Subscription;
  constructor(public appSettings:AppSettings,private spinner: NgxSpinnerService, public appService:AppService, public mediaObserver: MediaObserver, public router:Router, public loginService: LoginService, public snackBar: MatSnackBar,public translate: TranslateService) {
    this.settings = this.appSettings.settings;
    this.currentLoc = {
      id: 1,
      lat: 43.33916248737746,
      lng: -81.18072509765626};
    this.searchObj = {currentPage:0,
      propertyType:"",
      address:"",
      saleLease:"",
      propertyOption:"",
      parkingSpaces:"",
      basement1:"",
      washrooms:"",
      approxSquareFootageMin:"",
      approxSquareFootageMax:"",
      soldDays:"",
      daysOnMarket:"",
      listPriceMin:"",
      listPriceMax:"",
      bedRooms:[],
      latitude:"",
      longitude:"",
      openHouse:"",
      withSold: false,
      radius:"",
      userId:""};
    this.watcher = mediaObserver.media$.subscribe((change: MediaChange) => {
      if(change.mqAlias == 'xs') {
        this.viewCol = 100;
      }else if(change.mqAlias == 'sm'){
        this.viewCol = 50;
      }else if(change.mqAlias == 'md'){
        this.viewCol = 33.3;
      }else{
        this.viewCol = 25;
      }
    });
  }

  ngOnInit() {
    this.appService.currentMessage.subscribe(message => this.message = message);
    var self = this;
    navigator.geolocation.getCurrentPosition(function(position){
      self.currentLoc.lat = position.coords.latitude;
      self.currentLoc.lng = position.coords.longitude; 
      self.appService.setLocation(self.currentLoc);
      if (localStorage.getItem("searchedData")) {
        self.propertiesnew = JSON.parse(localStorage.getItem("searchedData"));
        self.pageNumber = parseInt(localStorage.getItem("pageNumber"));
        self.propertiesCount = parseInt(localStorage.getItem("totalProperties"));
        self.showLoader = false;
      } else {
        self.callAPIFromLoc();
      }
    }, function (error) {
      self.setDefaultLoc();
      if (localStorage.getItem("searchedData")) {
        self.propertiesnew = JSON.parse(localStorage.getItem("searchedData"));
        self.pageNumber = parseInt(localStorage.getItem("pageNumber"));
        self.propertiesCount = parseInt(localStorage.getItem("totalProperties"));
        self.showLoader = false;
      } else {
        self.callAPIFromLoc();
      }
    });
    this.showLoader = true;
    
    
  }

  callAPIFromLoc(){
    this.pageNumber = 1;
    this.showLoader = true;
    this.getPropertiesSearchList(this.appService.getSearchObject());
  }

  setDefaultLoc(){
    this.currentLoc.lat = 43.63943345;
    this.currentLoc.lng = -79.39972759226308;
    this.appService.setLocation(this.currentLoc);
  }

  ngAfterViewInit() {
    setTimeout(() => {
        this.appService.changeState("home");
    }, 500);
  }

  ngDoCheck(){
    if(this.settings.loadMore.load){     
      this.settings.loadMore.load = false;
      this.pageNumber = this.pageNumber + 1; 
      this.getPropertiesSearchList(this.appService.getSearchObject()); 
    }
  }

  ngOnDestroy(){
    this.resetLoadMore();
    this.watcher.unsubscribe();
  }

  public getPropertiesSearchList(obj:SearchObj){
    obj.bedRooms = Array.from(obj.bedRooms);
    obj.currentPage = this.pageNumber;
    if(this.appService.getLocation()){
      var temp:any = this.appService.getLocation();
      obj.latitude = temp.lat.toString();
      obj.longitude = temp.lng.toString();
    }else{
      obj.latitude = this.currentLoc.lat.toString();
      obj.longitude = this.currentLoc.lng.toString();
    }
    this.appService.searchProperty(obj).subscribe(data => {
      this.showLoader = false;
      this.showData(data);
      if(this.loginService.getUser()){
        this.appService.changeMessage("user");
      }else{
        this.appService.changeMessage("no");
      }
     })
  }

  showData(data:any){
    this.propertiesStatus = data;
    if(this.propertiesStatus.status == '200'){
      if(this.propertiesStatus.data && this.propertiesStatus.data.length == 0){ 
        this.snackBar.open('No Listing found in current area !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 }); 
        this.settings.loadMore.page++;
        this.pagination.page = this.settings.loadMore.page; 
      }
      let result = this.filterData(data.data);
      result.data = data.data;
      if(result.data.length == 0){
        this.propertiesnew = [];
        this.propertiesStatus.data.length = 0;
        this.pagination = new Pagination(1, this.count, null, 2, 0, 0);  
        this.message = 'No Results Found';
        return false;
      } 
      this.pagination = result.pagination;
      this.message = null;
      if(this.propertiesStatus.data.length == this.pagination.total){
        this.settings.loadMore.complete = true;
        this.settings.loadMore.result = this.propertiesStatus.data.length;
      }else{
        this.settings.loadMore.complete = false;
      }
      if(this.propertiesStatus.data){
        for (let i = 0; i < this.propertiesStatus.data.length; i++) {
          if(!this.propertiesStatus.data[i].propertyImage){
            this.propertiesStatus.data[i].gallery = [         
                {id:1,
                    "small": "assets/images/props/flat-1/1-small.jpg",
                    "medium": "assets/images/props/flat-1/1-medium.jpg",
                    "big": "assets/images/props/flat-1/1-big.jpg"
                }
            ];
          }else{
            this.propertiesStatus.data[i].gallery = [];
            this.propertiesStatus.data[i].gallery.push({id:1,
              "small": this.propertiesStatus.data[i].propertyImage,
              "medium": this.propertiesStatus.data[i].propertyImage,
              "big": "assets/images/props/flat-1/1-big.jpg"
            });
          }
          if(this.propertiesStatus.data[i].soldDate && this.propertiesStatus.data[i].saleLease == 'Sale'){
            this.propertiesStatus.data[i].saleLease = "Sold"
          }
          if(this.propertiesStatus.data[i].propertyStatus == undefined){
            if(this.propertiesStatus.data[i].soldDate){
              this.propertiesStatus.data[i].propertyStatus = ["Sold"]
            }else{
              if(this.propertiesStatus.data[i].saleLease == "Lease"){
                this.propertiesStatus.data[i].propertyStatus = ["For Sale"];
              }else{
                this.propertiesStatus.data[i].propertyStatus = ["For Rent"];
              }
            }
          }
        }  
      }
      this.propertiesCount = this.propertiesStatus?.count;
      if(this.pageNumber == 1){
        this.propertiesnew = this.propertiesStatus.data
      }else{
        this.propertiesnew = this.propertiesnew.concat(this.propertiesStatus.data);
      }
      localStorage.setItem("searchedData", JSON.stringify(this.propertiesnew));
      localStorage.setItem("pageNumber", this.pageNumber.toString());
      localStorage.setItem("totalProperties",this.propertiesCount);
    }
  }

  public resetLoadMore(){
    this.settings.loadMore.complete = false;
    this.settings.loadMore.start = false;
    this.settings.loadMore.page = 1;
    this.pagination = new Pagination(1, this.count, null, null, this.pagination.total, this.pagination.totalPages);
  }

  public filterData(data){
    return this.appService.filterData(data, undefined, this.sort, this.pagination.page, this.pagination.perPage);
  }

  public resetClicked(){
    this.searchFields = {currentPage:0, propertyType:"residential", address:"", saleLease:"Sale", propertyOption:"", parkingSpaces:"",
     basement1:"", washrooms:"", approxSquareFootageMin:"", approxSquareFootageMax:"", daysOnMarket:"", 
      soldDays:"", listPriceMin:"", listPriceMax:"", bedRooms:[], latitude:"", longitude:"", openHouse:"", withSold:false,  radius:"", userId:"",
      sliderControlGroup:null
    };
    this.getPropertiesSearchList(this.appService.getSearchObject());
  }

  public searchClicked(){ 
    this.appService.setSearchObject(this.searchFields);
    this.pageNumber = 1;
    this.showLoader = true;
    this.getPropertiesSearchList(this.appService.getSearchObject());
    //this.router.navigate(['/properties',this.searchObj]);
  }

  public loadMore(event){
  }

  public typeChanged(event){
    if(event != "Commercial"){
      this.searchObj.propertyType = "commercial";
    }else{
      this.searchObj.propertyType = "residential";
    }
  }

  public searchChanged(event){
    event.valueChanges.subscribe(() => {
      if(this.searchFields == undefined){
        this.searchFields = event.value;
      }
      if(event.value.myControl){
        this.searchFields.myControl = event.value.myControl;
      }
      if(event.value.propertyOption){
        this.searchFields.propertyOption = event.value.propertyOption;
      }else{
        this.searchFields.propertyOption = "";
      }
      if(event.value.propertyStyle){
        this.searchFields.propertyStyle = event.value.propertyStyle;
      }
      if(event.value.openHouse){
        this.searchFields.openHouse = event.value.openHouse;
      }
      if(event.value.bedrooms){
        this.searchFields.bedrooms = event.value.bedrooms;
      }
      if(event.value.propertyType && event.value.propertyType != ""){
        this.searchFields.propertyType = event.value.propertyType;
      }
      if(event.value.saleLease){
        this.searchFields.saleLease = event.value.saleLease;
      }
      if(event.value.basementStyle){
        this.searchFields.basementStyle = event.value.basementStyle;
      }
      if(event.value.washrooms){
        this.searchFields.washrooms = event.value.washrooms;
      }
      if(event.value.price.from){
        this.searchFields.price.from = event.value.price.from;
      }
      if(event.value.price.to){
        this.searchFields.price.to = event.value.price.to;
      }
      if(event.value.parkingSpaces != undefined && event.value.parkingSpaces != null){
        this.searchFields.parkingSpaces = event.value.parkingSpaces;
      }
      if(event.value.daysOnMarket){
        this.searchFields.daysOnMarket = event.value.daysOnMarket;
      }else{
        this.searchFields.daysOnMarket = "";
      }
      if(event.value.sliderControlGroup != null && event.value.sliderControlGroup.sliderControl != null){
        this.searchFields.sliderControlGroup = event.value.sliderControlGroup;
      }
     
      if(event.value.propertySoldDays){
        this.searchFields.propertySoldDays = event.value.propertySoldDays;
      }else{
        this.searchFields.propertySoldDays = "";
      }
      if(event.value.address){
        this.searchFields.address = event.value.address;
      }else{
        this.searchFields.address = "";
      }
    }); 
    event.valueChanges.pipe(debounceTime(500), distinctUntilChanged()).subscribe(() => {
      
    });        
  } 
  public removeSearchField(field){ 
    this.message = null;
    this.removedSearchField = field; 
  } 

  public changeCount(count){
    this.count = count;
    this.resetLoadMore();   
    //this.properties.length = 0;
  }

  public changeSorting(sort){    
    this.sort = sort;
    this.resetLoadMore(); 
    //this.properties.length = 0;
  }

  public changeViewType(obj){ 
    this.viewType = obj.viewType;
    this.viewCol = obj.viewCol; 
  }
}
