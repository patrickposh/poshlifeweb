import { Component, Inject } from '@angular/core';
import {  MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { Router } from '@angular/router';


@Component({
	selector: 'property-info-sheet',
	styleUrls: ['./property-info-sheet.scss'],
	templateUrl: './property-info-sheet.html',
})

export class PropertyInfoSheet {

	properties = [];
	imageUrl: any;

  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: any, public router: Router,
		private _bottomSheetRef: MatBottomSheetRef<PropertyInfoSheet>) {
      this.properties = data;
      this.imageUrl = 'https://property.mindnerves.com/api/property/map-image/';
      this._bottomSheetRef.disableClose = false;
  }

  openLink(event: MouseEvent, id): void {
		this._bottomSheetRef.dismiss();
		event.preventDefault();
		this.router.navigateByUrl('/properties/' + id);
  }
  
  closePopup(event: MouseEvent): void {
		this._bottomSheetRef.dismiss();
		event.preventDefault();
	}
}
