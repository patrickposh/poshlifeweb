import { Component, OnInit, ViewChild, HostListener, ViewChildren, QueryList, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { Property, UserData } from 'src/app/app.models';
import { SwiperConfigInterface, SwiperDirective } from 'ngx-swiper-wrapper';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { AppSettings, Settings } from 'src/app/app.settings';
import { CompareOverviewComponent } from 'src/app/shared/compare-overview/compare-overview.component';
import { EmbedVideoService } from 'ngx-embed-video'; 
import { emailValidator } from 'src/app/theme/utils/app-validators';
import { Meta } from '@angular/platform-browser';
import { DOCUMENT } from '@angular/common';
import { LabelType, Options } from 'ng5-slider';
import { filter, debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import { MatOptionSelectionChange } from '@angular/material/core';
import { LoginService } from 'src/app/login.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogFormModule } from 'src/app/shared/dialog-form/dialog-form.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { PropertyInfoSheet } from '../property-info-sheet.component';

@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.scss']
})
export class PropertyComponent implements OnInit {
  @ViewChild('sidenav') sidenav: any;  
  @ViewChildren(SwiperDirective) swipers: QueryList<SwiperDirective>;
  public psConfig: PerfectScrollbarConfigInterface = {
    wheelPropagation:true
  };
  public propertyOptionsComm = [];
  public sidenavOpen:boolean = true;
  public config: SwiperConfigInterface = {}; 
  public config2: SwiperConfigInterface = {}; 
  private sub: any;
  public property:Property;
  public settings: Settings;  
  public embedVideo: any;
  public relatedProperties: Property[];
  public relatedPropertiesForMap: Property[];
  public agent:any;
  public mortgageForm: FormGroup;
  public scheduleForm: FormGroup;
  public monthlyPayment:any;
  public dateDifference:number;
  public contactForm: FormGroup;
  public showLoader: boolean = false;
  public showSchoolsOnMap: boolean = true;
  public isForSale: boolean = false;
  public showMore: boolean = false;
  public nearBySold: boolean = false;
  public showMoreRooms: boolean = false;
  public searchValues = [];
  public bathrooms = [];
  public parkings = [];  
  public propertyOptionsRes = []; 
  public basementStyles = [];
  public bedrooms = new Set();
  public prTypeStatus = "";
  public mapLayer = "street";
  isLoading: boolean;
  public user:UserData;
  productId: string;
  value: number = 0;
  highValue: number = 0;
  public options: Options = {
    floor: 500,
    ceil: 20000
  };
  public optionsArea: Options = {
    floor: 1,
    ceil: 10
  };
  valueNew: number = 0;
  highValueNew: number = 0;
  valueArea: number = 1;
  highValueArea: number = 1;
  public optionsNew: Options = {
    floor: 500,
    ceil: 1000000,
    translate: (value: number, label: LabelType): string => {
      return '$' + value.toLocaleString('en');
    }
  };
  public validatingForm: FormGroup;
  public displayedColumns: string[] = ['room', 'Dimension', 'Description', 'level'];
  public displayedColumnsHistory: string[] = ['Start Date', 'End Date', 'Price', 'Event', 'listingID'];
  public searchoptions: any[] = [];
  imageUrl: any;

  public lat: number = 40.678178;
  public lng: number = -73.944158;
  public zoomLevel: number = 18; 
  public properties: Property[];
  public minClusterSize: number = 2;
  public origin: {};
  public destination: {};
  public currentLat:number;
  public currentLng:number;
  public isReadLess : boolean = false;
  public renderOptions = {
    suppressMarkers: true,
  }

  public markerOptions = {
    origin: {
            draggable: true,
          },
    destination: {
              opacity: 0.8,
          },
  }
  public clusterStyles = [
    {
      textColor: 'black',
      url: 'assets/images/m3.png',
      height: 36,
      width: 36
    },
   {
      textColor: 'white',
      url: 'assets/images/m3.png',
      height: 36,
      width: 36
    },
   {
      textColor: 'white',
      url: 'assets/images/m3.png',
      height: 36,
      width: 36
    }
  ];

  constructor(public appSettings:AppSettings, 
              public appService:AppService, 
              private activatedRoute: ActivatedRoute, 
              private embedService: EmbedVideoService,
              public loginService: LoginService, public snackBar: MatSnackBar,
              public fb: FormBuilder,
              private bottomSheet: MatBottomSheet,
              private meta: Meta,public dialog: MatDialog,
              @Inject(DOCUMENT) private document,public translate: TranslateService, public router:Router) {
    this.settings = this.appSettings.settings;
    this.relatedProperties = [];
    this.imageUrl = 'https://property.mindnerves.com/api/property/map-image/';
    this.meta.addTags([
      { name: 'twitter:card', content: 'summary_large_image' },
      { name: 'twitter:site', content: '@themeseason' },
      { name: 'twitter:title', content: 'Property Name' },
      { name: 'twitter:description', content: 'Property description' },
      { name: 'twitter:image', content: 'https://fakeimg.pl/600x400/' },
      { name: 'og:title', content: 'Property Name' },
      { name: 'og:description', content: 'Property description' },
      { name: 'og:image', content: 'https://fakeimg.pl/600x400/' },
      { name: 'og:url', content: 'http://themeseason.com' },
      { name: 'og:site_name', content: 'POSHLIFE' },
      { name: 'og:type', content: 'website' }
    ]); 
  }

  ngOnInit() {
    this.user = this.loginService.getUser();
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.productId = params['id'];
      this.getPropertynew(params['id']);
    });  
    this.propertyOptionsComm = this.appService.getPropertyTypesComm();
    this.propertyOptionsRes = this.appService.getPropertyTypesRes();
    this.basementStyles = this.appService.getBasementType();
    this.getAgent(1);
    if(window.innerWidth < 960){
      this.sidenavOpen = false;
      if(this.sidenav){
        this.sidenav.close();
      } 
    };
    this.mortgageForm = this.fb.group({
      principalAmount: ['', Validators.required],
      downPayment: ['', Validators.required], 
      interestRate: ['', Validators.required],
      period: ['', Validators.required]
    });
    this.scheduleForm = this.fb.group({
      name: ['', Validators.required],
      contactNumber: ['', Validators.required], 
      email: ['', Validators.required],
      message: ['', Validators.required],
      userId: null,
      propertyId: null
    });
    this.contactForm = this.fb.group({
      name: ['', Validators.required],
      sliderControlGroup: this.fb.group({
        sliderControl: new FormControl([500, 500]),
      }),
      sliderControlGroupArea : this.fb.group({
        sliderControlArea: new FormControl([0, 0]),
      }),
      sliderControlGroupNew: this.fb.group({
        sliderControlNew: new FormControl([0, 0]),
      }),
      email: ['', Validators.compose([Validators.required, emailValidator])],
      phone: null,
      propertyType: null,
      propertyOption: null,
      saleLease: null,
      bedrooms: [],
      washrooms: null,
      basementStyle: null,
      parkingSpaces: null,
      message: null,
      address: new FormControl()
    });
    this.contactForm.patchValue({
      propertyOption : "residential"  
    })
    this.validatingForm = new FormGroup({
      subscriptionFormModalName: new FormControl('', Validators.required),
      subscriptionFormModalEmail: new FormControl('', Validators.email)
    });
    this.prTypeStatus = "residential";
    this.contactForm.get("address").valueChanges
    .pipe(
      filter(text => text.length > 2),
      debounceTime(500),
      tap(() => {
        this.searchoptions = [];
        this.isLoading = true;
      }),
      switchMap(value => this.appService.searchAddress(this.getSearchFilt(value))
        .pipe(
          finalize(() => {
            //this.onSearchChange.emit(this.form);
            this.isLoading = false
          }),
        )
      )
    )
    .subscribe(data => {
      if (data.data == undefined) {
        this.searchoptions = [];
      } else {
        this.searchoptions = data.data; 
      }
    });
    this.appService.getRateOfInterest('RateOfInterest').subscribe(data => {
      if(data.code == 200 && data.status == "success"){
        this.mortgageForm.patchValue({
          interestRate : data.data.taxValue
        })
      }
    });
  }
  get subscriptionFormModalName() {
    return this.validatingForm.get('subscriptionFormModalName');
  }

  get subscriptionFormModalEmail() {
    return this.validatingForm.get('subscriptionFormModalEmail');
  }
  changeMapMarkers(value: boolean){
    this.showSchoolsOnMap = value;
    this.nearBySold = value;
    if(!value){  
      if(this.property.nearby && this.property.nearby.leaseProperties){
        this.loadnearByProperties(this.property.nearby.leaseProperties);
      }
    }else{
      if(this.property.nearby && this.property.nearby.soldProperties){
        this.loadnearByProperties(this.property.nearby.soldProperties);
      }
    }
  }

  clickOnCluster(cluster: any): void {
    var tempArr = [];
    if(this.zoomLevel == 18) {
      cluster.markers_.map(m => { 
        tempArr.push(m.title)
      });
      this.getMarkerProperties(tempArr);
    }
  }

  getMarkerProperties(data) {
    var markerProperties :any = [];
    this.relatedPropertiesForMap.forEach(element => {
      data.forEach(data => {
        if(data == element.id) {
          if(Array.isArray(element.propertyImage)){
            element.propertyImage.forEach(element1 => {
              element.propertyImage1 = element1;
            })
          }
          markerProperties.push(element);
        }
      });
    });
    this.openBottomSheet(markerProperties);
  }

  openBottomSheet(data): void {
		this.bottomSheet.open(PropertyInfoSheet, { data: data });
	}

  optionSelected(event: MatOptionSelectionChange) {
  }
  
  private getSearchFilt(text:string){
    return {"currentPage":1,"searchText":text}
  }
  
  ngOnDestroy() {
    this.sub.unsubscribe();
    this.meta.removeTag('name="twitter:card"');
    this.meta.removeTag('name="twitter:site"');
    this.meta.removeTag('name="twitter:title"');
    this.meta.removeTag('name="twitter:description"');
    this.meta.removeTag('name="twitter:image"');
    this.meta.removeTag('name="og:title"');
    this.meta.removeTag('name="og:description"');
    this.meta.removeTag('name="og:image"');
    this.meta.removeTag('name="og:url"');
    this.meta.removeTag('name="og:site_name"');
    this.meta.removeTag('name="og:type"');
  } 

  @HostListener('window:resize')
  public onWindowResize():void {
    (window.innerWidth < 960) ? this.sidenavOpen = false : this.sidenavOpen = true; 
  }

  public getPropertynew(id){
    this.showLoader = true;
    this.appService.getPropertynew(id).subscribe(data=>{
      this.property = data.data[0];
      this.showLoader = false;
      this.property.gallery = [];
      if(this.property.event == 'Terminated' && this.property.terminatedDate){
        let currentDate = new Date();
        var dateSent = new Date(this.property.terminatedDate);
        this.dateDifference = Math.floor((Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()) - Date.UTC(dateSent.getFullYear(), dateSent.getMonth(), dateSent.getDate()) ) /(1000 * 60 * 60 * 24));
      }
      if(this.property.propertyImage){
        this.property.propertyImage.forEach(element => {
          this.property.gallery.push({id:1,
            "small": element,
            "medium": element,
            "big": "assets/images/props/flat-1/1-big.jpg"
          });
        });
      }
      if(this.property.nearby && this.property.nearby.soldProperties){
        this.loadnearByProperties(this.property.nearby.soldProperties);
      }
      console.log("Details property06 ",this.property);
      setTimeout(() => { 
        this.config.observer = true;
        this.config2.observer = true; 
        this.swipers.forEach(swiper => { 
          if(swiper){
            swiper.setIndex(0);
          } 
        }); 
      });
      
      let port = (this.document.location.port) ? ':'+this.document.location.port+'/' : '/';   
      let url = this.document.location.protocol +'//'+ this.document.location.hostname + port;
      this.meta.updateTag({ name: 'twitter:title', content: this.property.title });
      this.meta.updateTag({ name: 'twitter:description', content: this.property.desc });
      //this.meta.updateTag({ name: 'twitter:image', content: url + this.property.gallery[0].medium });     
      this.meta.updateTag({ name: 'og:title', content: this.property.title });
      this.meta.updateTag({ name: 'og:description', content: this.property.desc });
      //this.meta.updateTag({ name: 'og:image', content: url + this.property.gallery[0].medium });

    });
  }

  loadnearByProperties(list:any[]){
    this.relatedProperties = [];
    this.relatedPropertiesForMap = [];
    for (var i = 0; i < list.length; i++) {
      list[i].gallery = [];
      list[i].propertyImage1 = list[i].propertyImage;
      if(list[i].soldDate && list[i].saleLease == 'Sale'){
        list[i].saleLease = "Sold";
      }
      if(list[i].propertyImage){
        if(list[i].propertyImage){
          list[i].gallery.push({id:1,
            "small": list[i].propertyImage,
            "medium": list[i].propertyImage,
            "big": "assets/images/props/flat-1/1-big.jpg"
          });
        }else{
          list[i].gallery.push({id:1,
            "small": "assets/images/props/flat-1/1-big.jpg",
            "medium": "assets/images/props/flat-1/1-big.jpg",
            "big": "assets/images/props/flat-1/1-big.jpg"
          });
        }
      }
      this.relatedPropertiesForMap.push(list[i]);
      this.relatedProperties.push(list[i]);
    }
    this.relatedPropertiesForMap.push(this.property);
  }

  ngAfterViewInit(){
    setTimeout(() => {
      this.appService.changeState("property");
    }, 500);
    this.config = {
      observer: false,
      slidesPerView: 1,
      spaceBetween: 0,       
      keyboard: true,
      navigation: true,
      pagination: false,
      grabCursor: true,        
      loop: false,
      preloadImages: false,
      lazy: true,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false
      }
    };

    this.config2 = {
      observer: false,
      slidesPerView: 4,
      spaceBetween: 16,      
      keyboard: true,
      navigation: false,
      pagination: false, 
      grabCursor: true,       
      loop: false, 
      preloadImages: false,
      lazy: true,  
      breakpoints: {
        480: {
          slidesPerView: 2
        },
        600: {
          slidesPerView: 3,
        }
      }
    }
  }
  

  public onOpenedChange(){ 
    this.swipers.forEach(swiper => { 
      if(swiper){
        swiper.update();
      } 
    });     
  }

  public selectImage(index:number){ 
    this.swipers.forEach(swiper => {
      if(swiper['elementRef'].nativeElement.id == 'main-carousel'){
        swiper.setIndex(index);
      }      
    }); 
  }

  public onIndexChange(index: number) {  
    this.swipers.forEach(swiper => { 
      let elem = swiper['elementRef'].nativeElement;
      if(elem.id == 'small-carousel'){
        swiper.setIndex(index);  
        for (let i = 0; i < elem.children[0].children.length; i++) {
          const element = elem.children[0].children[i]; 
          if(element.classList.contains('thumb-'+index)){
            element.classList.add('active-thumb'); 
          }else{
            element.classList.remove('active-thumb'); 
          }
        }
      } 
    });     
  }

  public addToCompare(){
    this.appService.addToCompare(this.property, CompareOverviewComponent, (this.settings.rtl) ? 'rtl':'ltr'); 
  }

  public onCompare(){
    return this.appService.Data.compareList.filter(item=>item.id == this.property.id)[0];
  }

  public addToFavorites(){
    this.appService.addToFavorites(this.property, (this.settings.rtl) ? 'rtl':'ltr');
  }

  public onFavorites(){
    return this.appService.Data.favorites.filter(item=>item.id == this.property.id)[0];
  }

  addBedrooms(type:string){
    if(this.bedrooms.has(type)){
      this.bedrooms.delete(type);
    }else if(type === 'All'){
      this.bedrooms.clear();
    }else{
      this.bedrooms.add(type);
    }
    this.contactForm.patchValue({
      bedrooms : this.bedrooms  
    })
  }

  addBathrooms(type:string){
    if(type == "All"){
      this.bathrooms = [];
      this.contactForm.patchValue({
        washrooms : "0"
      })
    }else{
      this.bathrooms = [];
      this.bathrooms.push(type);
      this.contactForm.patchValue({
        washrooms : this.bathrooms[0]
      })
    }
  }

  addParkings(type:string){
    this.parkings = [];
    this.parkings.push(type);
    this.contactForm.patchValue({
      parkingSpaces : this.parkings[0]
    })
  }

  changePrType(type:string){
    if(type == 'sale'){
      this.contactForm.patchValue({
        saleLease : "Sale"  
      })
      this.isForSale = true;
    }else if(type == 'rent'){
      this.contactForm.patchValue({
        saleLease : "Lease"  
      })
      this.isForSale = false;
    }
  }

  public getAgent(agentId:number = 1){
    var ids = [1,2,3,4,5]; //agent ids 
    agentId = ids[Math.floor(Math.random()*ids.length)]; //random agent id
    this.agent = this.appService.getAgents().filter(agent=> agent.id == agentId)[0]; 
  }

  public onContactFormSubmit(values:any){
    var beds :any;
    if(values.bedrooms){
      beds = Array.from(values.bedrooms); 
    }
    var temp = {
      "name": values.name,
      "email": values.email,
      "phone": values.phone,
      "message": values.message,
      "propertyOption": values.propertyOption,
      "propertyType": values.propertyType,
      "saleLease": values.saleLease,
      "washrooms": values.washrooms,
      "address": values.address,
      "basementStyle": values.basementStyle,
      "bedrooms": beds,
      "parkingSpaces": values.parkingSpaces,
      "areaMin": values.sliderControlGroup.sliderControl[0] == 500 ? '' : values.sliderControlGroup.sliderControl[0],
      "areaMax": values.sliderControlGroup.sliderControl[1] == 500 ? '' : values.sliderControlGroup.sliderControl[1],
      "areaRadius": values.sliderControlGroupArea.sliderControlArea[1],
      "priceMin": values.sliderControlGroupNew.sliderControlNew[0] == 500 ? '' : values.sliderControlGroupNew.sliderControlNew[0],
      "priceMax": values.sliderControlGroupNew.sliderControlNew[1] == 500 ? '' : values.sliderControlGroupNew.sliderControlNew[1],
      "isActive":true
      }
    this.showLoader = true;
    this.appService.subscribeToNewsLatter(temp).subscribe(data=>{
      this.contactForm.get('sliderControlGroup').patchValue({'sliderControl':[0,0]});
      this.contactForm.get('sliderControlGroupNew').patchValue({'sliderControlNew':[0,0]});
      this.parkings = [];
      this.bathrooms = [];
      this.prTypeStatus = "residential";
      this.bedrooms.clear();
      this.contactForm.reset();
      Object.keys(this.contactForm.controls).forEach(key => {
          this.contactForm.get(key).setErrors(null) ;
      });
      this.showLoader = false;
      this.snackBar.open('Successfully subscribed to daily updates !', '×', { panelClass: 'success', verticalPosition: 'top', duration: 3000 });
    }) 
  }

  public onMortgageFormSubmit(values:Object){ 
    if (this.mortgageForm.valid) { 
      var principalAmount = values['principalAmount']
      var down = values['downPayment']
      var interest = values['interestRate']
      var term = values['period']
      // this.mortgageForm.reset();
      // Object.keys(this.mortgageForm.controls).forEach(key => {
      //     this.mortgageForm.get(key).setErrors(null) ;
      // });
      this.monthlyPayment = this.calculateMortgage(principalAmount, down, interest / 100 / 12, term * 12).toFixed(2);
    }     
  }

  public onScheduleFormSubmit(values:any){ 
    values.userId = this.loginService.getUser().userVM.id;
    values.propertyId = this.property.id;
    if(this.scheduleForm.valid){
      this.showLoader = true;
      this.appService.scheduleVisit(values).subscribe(data=>{
        this.scheduleForm.reset();
        this.showLoader = false;
        this.snackBar.open('Successfully Scheduled Visit !', '×', { panelClass: 'success', verticalPosition: 'top', duration: 3000 });
        Object.keys(this.scheduleForm.controls).forEach(key => {
            this.scheduleForm.get(key).setErrors(null) ;
        },err => {
          if(err.status == 401){
            this.snackBar.open('Session Expired !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
            setTimeout(() => {
                this.router.navigate(['/login']);
            }, 5000);
          }
        }); 
      })
    }
  }

  public calculateMortgage(principalAmount:any, downPayment:any, interestRate:any, period:any){    
    return ((principalAmount-downPayment) * interestRate) / (1 - Math.pow(1 + interestRate, -period));
  } 

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogFormModule, {
      width: '350px',
      data: {name: this.user.userVM.email, pid: this.property.id}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed',result);
    });
  }

  getCombinedDesc(element){
    let desc = '';
    for (const [key, value] of Object.entries(element)) {
      if(key.includes('roomDesc')){
        if(desc != ''){
            if(value != null)
            desc = desc + (value ? ', ' : '')+(value ? value : '');
        }else{
          if(value != null)
          desc = value+'';
        }
      }
    }
    return desc;
  }
  getCombinedConstruction(element){
    let desc = '';
    for (const [key, value] of Object.entries(element)) {
      if(key.includes('exterior')){
        if(desc != ''){
           desc = desc + (value ? ', ' : '')+(value ? value : '');
        }else{
          desc = value+'';
        }
      }
    }
    return desc;
  }

  roundOffNumber(number){
    if(number){
      return Math.round(number);
    }else{
      return 0;
    }
  }
  getConcatedString(data){
    return Array.prototype.map.call(data, function(item) { return item.value; }).join(", ");
  }

  getParkingSpaces(parkingList){
    let par = parkingList.find(obj => (obj.name == 'parkingSpaces'));
    if(par){
      return parseInt(par.value);
    }else{
      return 0;
    }
  }

  checkNull(obj){
    if(obj){
      return obj;
    }else{
      return '';
    }
  }

  toFixedRoomDimension(element){
    let roomLength = "0.0";
    let roomWidth = "0.0";
    if(element.roomLength){
      roomLength = (parseFloat(element.roomLength) * 3.28).toFixed(1);
    }
    if(element.roomWidth){
      roomWidth = (parseFloat(element.roomWidth) * 3.28).toFixed(1);
    }
    if(roomLength == '0.0' && roomWidth == '0.0'){
      return ''
    }else{
      return roomLength+' X '+roomWidth+' feet';
    } 
  }

  getDiff(day1, day2){
    if(!day1){
      day1 = 0;
    }
    if(!day2){
      day2 = 0;
    }
    return day1 - day2;
  }
}
