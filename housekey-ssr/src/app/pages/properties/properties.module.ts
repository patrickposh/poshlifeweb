import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core'; 
import { MatVideoModule } from 'mat-video';
import { SharedModule } from '../../shared/shared.module';
import { PropertiesComponent } from './properties.component';
import { PropertyInfoSheet } from './property-info-sheet.component';
import { PropertyComponent } from './property/property.component';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { Ng5SliderModule } from 'ng5-slider';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatCarouselModule } from '@ngmodule/material-carousel';
import { TranslateModule } from '@ngx-translate/core';
export const routes = [
  { path: '', component: PropertiesComponent, pathMatch: 'full' },
  { path: ':id', component: PropertyComponent }
];

@NgModule({
  declarations: [
    PropertiesComponent, 
    PropertyComponent,
    PropertyInfoSheet
  ],
  exports: [
    PropertiesComponent, 
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    AgmCoreModule,
    MatVideoModule,
    SharedModule,
    Ng5SliderModule,
    MatBottomSheetModule,
    AgmJsMarkerClustererModule,
    TranslateModule,
    MatCarouselModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCHCXkjFkYDOeqnO4Gwos55C73UmcaZdCw'
    })
  ],
  entryComponents: [
    PropertyInfoSheet
  ]
})
export class PropertiesModule { }
