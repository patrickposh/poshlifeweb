import { Component, OnInit, ViewChild, Inject, PLATFORM_ID, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { Subscription } from 'rxjs'; 
import { debounceTime, distinctUntilChanged } from 'rxjs/operators'; 
import { Settings, AppSettings } from '../../app.settings';
import { AppService } from '../../app.service';
import { Property, Pagination, SearchObj, LocationC } from '../../app.models'; 
import { isPlatformBrowser } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { PropertyInfoSheet } from './property-info-sheet.component';
import { get } from 'scriptjs';
declare var klokantech;
@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.scss']
})
export class PropertiesComponent implements OnInit {
  @ViewChild('sidenav') sidenav: any;
  public sidenavOpen:boolean = true;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  public psConfig: PerfectScrollbarConfigInterface = {
    wheelPropagation:true
  };
  public properties: Property[];
  public propertiesnew: Property[];
  public viewType: string = 'map';
  public searchObj: any;
  public searchObjNew: any;
  public viewCol: number = 33.3;
  public count: number = 12;
  public sort: string;
  public minClusterSize: number = 2;
  public searchFields: any;
  public removedSearchField: string;
  public pagination:Pagination = new Pagination(1, this.count, null, 2, 0, 0); 
  public message:string;
  public watcher: Subscription;
  private sub: any;
  private zoomLevel: any;
  public showSchools:boolean = false;
  public mapLoaded:boolean = false;
  public schoolData: any;
  public mapLayer = "street";
  public settings: Settings;
  public origin: {};
  public map: any;
  public currentLoc:LocationC;
  public showLoader: boolean = true;
  public destination: {};
  public renderOptions = {
    suppressMarkers: true,
  }

  public markerOptions = {
    origin: {
            draggable: true,
          },
    destination: {
              opacity: 0.8,
          },
  }
  public clusterStyles = [
    {
      textColor: 'black',
      url: 'assets/images/m3.png',
      height: 36,
      width: 36
    },
   {
      textColor: 'white',
      url: 'assets/images/m3.png',
      height: 36,
      width: 36
    },
   {
      textColor: 'white',
      url: 'assets/images/m3.png',
      height: 36,
      width: 36
    }
  ];

  constructor(public appSettings:AppSettings, private activatedRoute: ActivatedRoute, 
              public appService:AppService, 
              public mediaObserver: MediaObserver,
              private bottomSheet: MatBottomSheet,
              @Inject(PLATFORM_ID) private platformId: Object, public snackBar: MatSnackBar,public translate: TranslateService) {
    this.settings = this.appSettings.settings;    
    this.watcher = mediaObserver.media$.subscribe((change: MediaChange) => { 
      if (change.mqAlias == 'xs') {
        this.sidenavOpen = false;
        this.viewCol = 100;
      }
      else if(change.mqAlias == 'sm'){
        this.sidenavOpen = false;
        this.viewCol = 50;
      }
      else if(change.mqAlias == 'md'){
        this.viewCol = 50;
        this.sidenavOpen = true;
      }
      else{
        this.viewCol = 33.3;
        this.sidenavOpen = true;
      }
    });
    this.currentLoc = {
      id: 1,
      lat: 43.63943345,
      lng: -79.39972759226308};
  }

  ngOnInit() {
    var self = this;
    navigator.geolocation.getCurrentPosition(function(position){
      self.currentLoc.lat = position.coords.latitude;
      self.currentLoc.lng = position.coords.longitude;
      self.appService.setLocation(self.currentLoc);
    }, function (error) {
      self.setDefaultLoc();
    });
    this.zoomLevel = 14;
    this.sub = this.activatedRoute.params.subscribe(params=> {
      this.searchObjNew = params;
      this.searchObj = {currentPage:0,
        propertyType:this.searchObjNew.propertyType,
        address:this.searchObjNew.address,
        saleLease:this.searchObjNew.saleLease,
        propertyOption:this.searchObjNew.propertyOption,
        parkingSpaces:this.searchObjNew.parkingSpaces,
        basement1:this.searchObjNew.basement1,
        washrooms:this.searchObjNew.washrooms,
        approxSquareFootageMin:this.searchObjNew.approxSquareFootageMin,
        approxSquareFootageMax:this.searchObjNew.approxSquareFootageMax,
        daysOnMarket:this.searchObjNew.daysOnMarket,
        listPriceMin:this.searchObjNew.listPriceMin,
        listPriceMax:this.searchObjNew.listPriceMax,
        soldDays:this.searchObjNew.propertySoldDays,
        openHouse:this.searchObjNew.openHouse,
        withSold:false,
        radius:""};
    });
    if(this.searchObj){
      if(Object.keys(this.searchObjNew).length != 0){
        if(!Array.isArray(this.searchObj.propertyType)){
          this.searchObj.propertyType = this.searchObj.propertyType;
        }
        this.propertiesnew = [];
        this.viewType = 'map';
        this.searchObj = this.appService.getSearchObject();
      }else{
        this.propertiesnew = [];
      }
    }else{
      this.propertiesnew = [];
    }
    this.getPropertiesSearchList();
  }

  setDefaultLoc(){
    if(!this.appService.getLocation()){
      this.currentLoc.lat = 43.63943345;
      this.currentLoc.lng = -79.39972759226308;
      this.appService.setLocation(this.currentLoc);
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
        this.appService.changeState("properties");
    }, 500);
  }

  mapReady(map) {
    this.map = map;
    var self = this;
    this.map.addListener("dragend", function () {
      self.appService.setLocation(self.currentLoc);
      self.getPropertiesSearchList();
    });
    this.showLoader = false;
    this.map.addListener("tilesloaded", function () {
      this.mapLoaded = true;
    });
  }

  centerChange(event){
    // this.currentLoc.lat = event.lat;
    // this.currentLoc.lng = event.lng;
  }

  zoomChange(event){
    if(this.zoomLevel && this.zoomLevel != event){
      // && event < this.zoomLevel
      if(event < 17){
        this.zoomLevel = event;
        this.getPropertiesSearchList();
      }
      this.zoomLevel = event;
    };
  }

  toggleFunction(){
  }

  ngOnDestroy(){ 
    this.watcher.unsubscribe();
  }

  public getPropertiesSearchList(){
    this.mapLoaded = false;
    this.searchObj = this.appService.getSearchObject();
    if(this.searchObj){
      this.searchObj.bedRooms = Array.from(this.searchObj.bedRooms);
      this.searchObj.currentPage = 0;
      if(this.searchObj.propertyType == ""){
        this.searchObj.propertyType = "residential";
      }
      
      if(this.appService.getLocation()){
        var temp:any = this.appService.getLocation();
        this.searchObj.latitude = temp.lat.toString();
        this.searchObj.longitude = temp.lng.toString();
      }else{
        this.searchObj.latitude = this.currentLoc.lat.toString();
        this.searchObj.longitude = this.currentLoc.lng.toString();
      }
      this.searchObj.radius = this.appService.getRadius(this.zoomLevel);
      this.appService.searchPropertyMap(this.searchObj).subscribe(data => {
        if(data.data.length > 0){
          this.mapLoaded = true;
          this.getProperties(data);
        }else{
          this.showLoader = false;
          this.snackBar.open('No Listing found in current area !', '', { panelClass: 'success', verticalPosition: 'top', duration: 5000 });
          this.propertiesnew.length = 0;
          this.pagination = new Pagination(1, this.count, null, 2, 0, 0);  
          this.message = 'No Results Found';
          return false;
        }
      })
    }
  }

  public getProperties(data:any){
    this.propertiesnew = [];
    this.propertiesnew = data.data;
    if(this.propertiesnew){
      for (let i = 0; i < this.propertiesnew.length; i++) {
        if(this.propertiesnew[i].propertyStatus == undefined){
          if(this.propertiesnew[i].saleLease == "Lease"){
            this.propertiesnew[i].propertyStatus = ["For Sale"];
          }else{
            this.propertiesnew[i].propertyStatus = ["For Rent"];
          }
        }
      }  
    }
    if(this.appService.getLocation()){
      var temp:any = this.appService.getLocation();
      this.currentLoc.lat = temp.lat;
      this.currentLoc.lng = temp.lng;
    }
  }

  public resetPagination(){ 
    if(this.paginator){
      this.paginator.pageIndex = 0;
    }
    this.pagination = new Pagination(1, this.count, null, null, this.pagination.total, this.pagination.totalPages);
  }

  public filterData(data){
    return this.appService.filterData(data, this.searchFields, this.sort, this.pagination.page, this.pagination.perPage);
  }

  public resetClicked(){
    this.searchFields = {currentPage:0, propertyType:"residential", address:"", saleLease:"Sale", propertyOption:"", parkingSpaces:"",
     basement1:"", washrooms:"", approxSquareFootageMin:"", approxSquareFootageMax:"", daysOnMarket:"", 
      soldDays:"", listPriceMin:"", listPriceMax:"", bedRooms:[], latitude:"", longitude:"", openHouse:"", withSold:false,  radius:"", userId:"",
      sliderControlGroup:null
    };
    this.propertiesnew = [];
    this.getPropertiesSearchList();
  }
  
  public searchClicked(){
    this.appService.setSearchObject(this.searchFields);
    this.searchObj = this.appService.getSearchObject();
    if(Object.keys(this.searchObj).length != 0){
      this.propertiesnew = [];
      this.getPropertiesSearchList();
    }else{
      this.propertiesnew = [];
    }
    if (isPlatformBrowser(this.platformId)) {
      window.scrollTo(0,0);
    } 
  }

  public typeChanged(event){
    if(event != ""){
      this.searchObj.propertyType = event;
    }
    this.appService.setSearchObject(this.searchFields);
  }

  public searchChanged(event){
    event.valueChanges.subscribe(() => {
      if(this.searchFields == undefined){
        this.searchFields = event.value;
      }
      this.resetPagination(); 
      if(event.value.myControl){
        this.searchFields.myControl = event.value.myControl;
      }
      if(event.value.propertyOption){
        this.searchFields.propertyOption = event.value.propertyOption;
      }else{
        this.searchFields.propertyOption = "";
      }
      if(event.value.propertyStyle){
        this.searchFields.propertyStyle = event.value.propertyStyle;
      }
      if(event.value.openHouse){
        this.searchFields.openHouse = event.value.openHouse;
      }
      if(event.value.bedrooms){
        this.searchFields.bedrooms = event.value.bedrooms;
      }
      if(event.value.propertyType && event.value.propertyType != ""){
        this.searchFields.propertyType = event.value.propertyType;
      }
      if(event.value.saleLease){
        this.searchFields.saleLease = event.value.saleLease;
      }
      if(event.value.basementStyle){
        this.searchFields.basementStyle = event.value.basementStyle;
      }
      if(event.value.washrooms){
        this.searchFields.washrooms = event.value.washrooms;
      }
      if(event.value.price.from){
        this.searchFields.price.from = event.value.price.from;
      }
      if(event.value.price.to){
        this.searchFields.price.to = event.value.price.to;
      }
      if(event.value.parkingSpaces != undefined && event.value.parkingSpaces != null){
        this.searchFields.parkingSpaces = event.value.parkingSpaces;
      }
      if(event.value.daysOnMarket){
        this.searchFields.daysOnMarket = event.value.daysOnMarket;
      }else{
        this.searchFields.daysOnMarket = "";
      }
      if(event.value.sliderControlGroup != null && event.value.sliderControlGroup.sliderControl != null){
        this.searchFields.sliderControlGroup = event.value.sliderControlGroup;
      }
     
      if(event.value.propertySoldDays){
        this.searchFields.propertySoldDays = event.value.propertySoldDays;
      }else{
        this.searchFields.propertySoldDays = "";
      }
      if(event.value.address){
        this.searchFields.address = event.value.address;
      }else{
        this.searchFields.address = "";
      }
      setTimeout(() => {      
        this.removedSearchField = null;
      });
      if(!this.settings.searchOnBtnClick){     
        this.properties.length = 0;  
      }         
    });
    event.valueChanges.pipe(debounceTime(500), distinctUntilChanged()).subscribe(() => { 
      if(this.zoomLevel && this.mapLoaded){
        // this.getPropertiesSearchList(); 
      }
    });       
  } 

  public removeSearchField(field){ 
    this.message = null;   
    this.removedSearchField = field; 
  } 

  public changeCount(count){
    this.count = count;   
    this.properties.length = 0;
    this.resetPagination();
  }
  public changeSorting(sort){    
    this.sort = sort; 
    this.properties.length = 0;
  }
  public changeViewType(obj){
    if(obj.viewType == 'map'){
      if(Object.keys(this.searchObjNew).length != 0){
        this.propertiesnew = [];
        this.searchObj = this.appService.getSearchObject();
      }else{
        this.propertiesnew = [];
      }
    }
    this.viewType = obj.viewType;
    this.viewCol = obj.viewCol;
  } 

  public onPageChange(e){ 
    this.pagination.page = e.pageIndex + 1;
    if (isPlatformBrowser(this.platformId)) {
      window.scrollTo(0,0);
    } 
  }

  protected mapLoad(map) {
    this.renderGeolocationControl(map);
  }

  renderGeolocationControl(map) {
    // get('https://cdn.klokantech.com/maptilerlayer/v1/index.js', () => {
    //   const geoloccontrol = new klokantech.GeolocationControl(map, 18);
    // });
  }

  clickOnCluster(cluster: any): void {
    var tempArr = [];
    if(this.zoomLevel == 18) {
      cluster.markers_.map(m => { 
        tempArr.push(m.title)
      });
      this.getMarkerProperties(tempArr);
    }
  }
  markerClicked(event): void {
    this.propertiesnew.forEach(element => {
        if(event.title == element.id) {
          element.propertyImage1 = this.appService.propertyImageURl + element.propertyImage+"-1.jpeg";
          this.openBottomSheet([element]);
        }
    });
  }
  

  getMarkerProperties(data) {
    var markerProperties :any = [];
    this.propertiesnew.forEach(element => {
      data.forEach(data => {
        if(data == element.id) {
          element.propertyImage1 = this.appService.propertyImageURl + element.propertyImage+"-1.jpeg";
          markerProperties.push(element);
        }
      });
    });
    this.openBottomSheet(markerProperties);
  }

  openBottomSheet(data): void {
		this.bottomSheet.open(PropertyInfoSheet, { data: data });
	}
}
