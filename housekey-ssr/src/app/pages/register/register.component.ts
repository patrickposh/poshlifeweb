import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { matchingPasswords, emailValidator } from 'src/app/theme/utils/app-validators';
import { LoginService } from '../../login.service';
import { TranslateService } from '@ngx-translate/core';
import { TermsConditionModule } from '../../shared/terms-condition/terms-condition.component';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public registerForm: FormGroup;
  public hide = true;
  public showLoader: boolean = false;
  public registrationFlag = false;
  public registrationMsg: String = "";
  public registrationClass: String = "";
  public userTypes = [
    { id: 1, name: 'Agent' },
    { id: 2, name: 'Agency' },
    { id: 3, name: 'Buyer' }
  ];
  constructor(public fb: FormBuilder, public router: Router, public snackBar: MatSnackBar, private loginService: LoginService,public translate: TranslateService, public dialog: MatDialog) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      firstname: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      lastname: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      email: ['', Validators.compose([Validators.required, emailValidator])],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      phoneNumber: ['', Validators.compose([Validators.required, Validators.minLength(9)])]          
    }, { validator: matchingPasswords('password', 'confirmPassword') });
  }

  resetFields(){
    this.registerForm.reset();
    Object.keys(this.registerForm.controls).forEach(key => {
      this.registerForm.get(key).setErrors(null) ;
    });
    setTimeout(() => {
      this.router.navigate(['/login']);
    }, 5000);
  }

  public onRegisterFormSubmit(values: Object): void {
    if (this.registerForm.valid) {
      this.registrationFlag = true;
      var user = values;
      user['comment'] = '';
      user['createdBy'] = '';
      user['createdDate'] = '';
      user['modifiedDate'] = '';
      delete user['confirmPassword'];
      this.showLoader = true;
      this.loginService.register(user).subscribe(data => {
        this.showLoader = false;
        this.registrationMsg = data.message;
        this.snackBar.open(data.message, '×', { panelClass: 'success', verticalPosition: 'top', duration: 9000 });
        if (data.errorKey == 'noError' && data.status == 'success') {  
          this.registrationClass = 'Activation link has been sent to your email.';
        } else {
          this.registrationClass = 'registerFailure';
        }
        this.resetFields();
      });
    }
  }
  openTermsDialog(): void {
    const dialogRef = this.dialog.open(TermsConditionModule);
  }
}
